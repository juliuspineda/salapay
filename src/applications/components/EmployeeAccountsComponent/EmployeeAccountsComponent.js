import React, {useState} from 'react'
import {
    Grid,
    Toolbar,
    Typography,
    Avatar
} from '@material-ui/core';

import HeaderComponent from '../../components/HeaderComponent/HeaderComponent';
import BreadCrumbsComponent from '../../components/BreadCrumbsComponent/BreadCrumbsComponent';

import { makeStyles } from '@material-ui/core/styles';
import CustomPaper from '../../Shared/CustomPaper/CustomPaper';
import CustomTabs from '../../Shared/CustomTabs/CustomTabs';
import CustomContainer from '../../Shared/CustomContainer/CustomContainer';

// import TitleContainerComponent from '../TitleContainerComponent/TitleContainerComponent';
import imgAvatar from '../../assets/images/avatar1.png';

const useStyles = makeStyles((theme) => ({
    content: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.default,
        padding: theme.spacing(3),
    },
    text: {
        margin: '.25rem 0rem .25rem 0rem',
        fontSize: '15px'
    },
    label: {
        margin: '.25rem .5rem .25rem 0rem',
        fontSize: '15px',
        fontWeight: 600
    },
    contentSubTitle: {
        fontSize: '12px',
        fontWeight: 'bold',
        padding: '0.5rem 0rem'
    },
    contentSubTitleContainer: {
        fontSize: '12px',
        fontWeight: 'bold',
        padding: '0.5rem 0rem',
        color: '#888'
    },
    contentSubTitleValue: {
        fontSize: '12px',
        padding: '0.5rem 0rem'
    },
    bold: {
        fontWeight: 'bold'
    },
    avatarStyle: {
        width: '5rem', 
        height: '5rem'
    }
}));

const POSITION = 'Regular Employee';
const EMPLOYEE_ID = '200223123';

const EARNINGS = {
    basicRate: 10000,
    otRate: 30,
    allowance: 1500,
    nightDifferential: 10
}

const DEDUCTIONS = {
    sss: 800,
    pagIbig: 300,
    philHealth: 150,
    hdmf: 50,
    cashBond: 0,
    deathContribution: 200,
    sssSalaryLoan: 0
}

const LABEL_TABS = {
    PROFILE: 'Personal Information',
    SALARY: 'Salary Information',
    CONTACT: 'Emergency Contact'
}

const PERSONAL_INFORMATION = {
    SSS: 'SSS ID',
    PAGIBIG: 'PAG-IBIG ID',
    TIN: 'TIN ID',
    PHONE: 'PHONE',
    NATIONALITY: 'NATIONALITY',
    RELIGION: 'RELIGION',
    MARITAL_STATUS: 'MARITAL STATUS'
}

const PERSONAL_INFORMATION_VALUE = {
    SSS: '1-400123-2323',
    PAGIBIG: '5012-12323-1',
    TIN: '123-2354-213',
    PHONE: '09151523235',
    NATIONALITY: 'Filipino',
    RELIGION: 'Christian',
    MARITAL_STATUS: 'Single'
}

const contactsArray = [
    {
        id: 1,
        title: 'Primary',
        name: 'Darryl Santos',
        relationship: 'Father',
        phone: '09152323111'
    },
    {
        id: 2,
        title: 'Secondary',
        name: 'Emy Santos',
        relationship: 'Mother',
        phone: '09152323100'
    },
];

const SALARY_TITLE_LABEL = {
    SALARY: 'Salary',
    DEDUCTIONS: 'Deductions'
}

const SALARY_LABEL = {
    EARNINGS: {
        BASIC_RATE: 'Basic Rate',
        OT_RATE: 'OT Rate',
        ALLOWANCE: 'Allowance',
        NIGHT_DIFF: 'Night Differential'
    },
    DEDUCTIONS: {
        SSS: 'SSS',
        PAG_IBIG: 'Pag-Ibig',
        PHIL_HEALTH: 'Philhealth',
        HDMF: 'HDMF MPL',
        CASH_BOND: 'Cash Bond',
        DEATH_CONTRI: 'Death Contribution',
        SSS_LOAN: 'SSS Salary Loan'
    }
}

const SALARY_VALUE = {
    earnings: {
        basicRate: 0.00,
        otRate: 0.00,
        allowance: 0.00,
        nightDiff: 0.00
    },
    deductions: {  
        sss: 0.00,
        pagIbig: 0.00,
        philHealth: 0.00,
        hdmfMPL: 0.00,
        cashBond: 0.00,
        deathContri: 0.00,
        sssSalaryLoan: 0.00
    } 
} 

const bgColor = '#ededed';

function EmployeeAccountsComponent(props) {
    const { data } = props.location.state;
    const [isTabs, setIsTabs] = useState(0);
    const classes = useStyles();

    const handleChange = (event, newValue) => {
        setIsTabs(newValue);
    };

    return(
        <div>
            <Grid item xs={12} sm={12} md={12} lg={12} className={classes.content} style={{ padding: '0px' }}>
                <Toolbar/>
                {/* <HeaderComponent /> */}
            </Grid>
            <CustomContainer>
                <Grid xs={12} sm={12} md={12} lg={12}>
                    <Grid container spacing={2}>
                        <Grid item xs={12} sm={12} md={12} lg={12}>
                            <BreadCrumbsComponent title="Profile" label="Employee" secondLabel="Account" hasAccount/>
                        </Grid>
                        <Grid item xs={12} sm={12} md={12} lg={12}>
                            <Grid container>
                                <CustomPaper borderClass={'5px 5px 0px 0px'}>
                                    <Grid container fullWidth>
                                        <Grid item xs>
                                            <Avatar src={imgAvatar} className={classes.avatarStyle} />
                                        </Grid>
                                        <Grid item xs={11}>
                                            <Grid container direction="column">
                                                <Typography variant="h5"><b>{data.firstName + ' ' + data.middleInitial + '. ' + data.lastName}</b></Typography>
                                                <Typography variant="subtitle1">Employee ID: {EMPLOYEE_ID}</Typography>
                                                <Typography variant="subtitle1">Position: {POSITION}</Typography>
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                </CustomPaper>
                                <CustomPaper borderClass={'0px 0px 5px 5px'} marginBottom isTabs>
                                    <CustomTabs value={isTabs} onChange={handleChange} />
                                </CustomPaper>
                                { 
                                    isTabs === 0 && <Grid container spacing={4}>
                                        <Grid item xs>
                                            <CustomPaper>
                                                <Grid container direction="column" style={{maxHeight: '335px', height: '335px'}}>
                                                    <Grid container>
                                                        <Grid item xs={12}>
                                                            <Typography variant="h6" className={classes.bold} gutterBottom>{LABEL_TABS.PROFILE}</Typography>
                                                        </Grid>
                                                        <Grid item xs={6}>
                                                            <Typography variant="subtitle1" className={classes.contentSubTitle}>{PERSONAL_INFORMATION.SSS}</Typography>
                                                        </Grid>
                                                        <Grid item xs={6}>
                                                            <Typography variant="subtitle1" className={classes.contentSubTitleValue}>{PERSONAL_INFORMATION_VALUE.SSS}</Typography>
                                                        </Grid>
                                                        <Grid item xs={6}>
                                                            <Typography variant="subtitle1" className={classes.contentSubTitle}>{PERSONAL_INFORMATION.PAGIBIG}</Typography>
                                                        </Grid>
                                                        <Grid item xs={6}>
                                                            <Typography variant="subtitle1" className={classes.contentSubTitleValue}>{PERSONAL_INFORMATION_VALUE.PAGIBIG}</Typography>
                                                        </Grid>
                                                        <Grid item xs={6}>
                                                            <Typography variant="subtitle1" className={classes.contentSubTitle}>{PERSONAL_INFORMATION.TIN}</Typography>
                                                        </Grid>
                                                        <Grid item xs={6}>
                                                            <Typography variant="subtitle1" className={classes.contentSubTitleValue}>{PERSONAL_INFORMATION_VALUE.TIN}</Typography>
                                                        </Grid>
                                                        <Grid item xs={6}>
                                                            <Typography variant="subtitle1" className={classes.contentSubTitle}>{PERSONAL_INFORMATION.PHONE}</Typography>
                                                        </Grid>
                                                        <Grid item xs={6}>
                                                            <Typography variant="subtitle1" className={classes.contentSubTitleValue}>{PERSONAL_INFORMATION_VALUE.PHONE}</Typography>
                                                        </Grid>
                                                        <Grid item xs={6}>
                                                            <Typography variant="subtitle1" className={classes.contentSubTitle}>{PERSONAL_INFORMATION.NATIONALITY}</Typography>
                                                        </Grid>
                                                        <Grid item xs={6}>
                                                            <Typography variant="subtitle1" className={classes.contentSubTitleValue}>{PERSONAL_INFORMATION_VALUE.NATIONALITY}</Typography>
                                                        </Grid>
                                                        <Grid item xs={6}>
                                                            <Typography variant="subtitle1" className={classes.contentSubTitle}>{PERSONAL_INFORMATION.RELIGION}</Typography>
                                                        </Grid>
                                                        <Grid item xs={6}>
                                                            <Typography variant="subtitle1" className={classes.contentSubTitleValue}>{PERSONAL_INFORMATION_VALUE.RELIGION}</Typography>
                                                        </Grid>
                                                        <Grid item xs={6}>
                                                            <Typography variant="subtitle1" className={classes.contentSubTitle}>{PERSONAL_INFORMATION.MARITAL_STATUS}</Typography>
                                                        </Grid>
                                                        <Grid item xs={6}>
                                                            <Typography variant="subtitle1" className={classes.contentSubTitleValue}>{PERSONAL_INFORMATION_VALUE.MARITAL_STATUS}</Typography>
                                                        </Grid>
                                                    </Grid>
                                                </Grid>
                                            </CustomPaper>
                                        </Grid>
                                        <Grid item xs>
                                            <CustomPaper>
                                            <Grid container direction="column" style={{maxHeight: '335px', height: '335px'}}>
                                                    <Grid container>
                                                        <Grid item xs={12}>
                                                            <Typography variant="h6" className={classes.bold} gutterBottom>{LABEL_TABS.CONTACT}</Typography>
                                                        </Grid>
                                                        {
                                                            contactsArray.map((contact, i) => {
                                                                return <Grid container>
                                                                    <Grid item xs={12}>
                                                                        <Typography variant="subtitle1" className={classes.contentSubTitleContainer}>{contact.title}</Typography>
                                                                    </Grid>
                                                                    <Grid item xs={6}>
                                                                        <Typography variant="subtitle1" className={classes.contentSubTitle}>Name</Typography>
                                                                    </Grid>
                                                                    <Grid item xs={6}>
                                                                        <Typography variant="subtitle1" className={classes.contentSubTitleValue}>{contact.name}</Typography>
                                                                    </Grid>
                                                                    <Grid item xs={6}>
                                                                        <Typography variant="subtitle1" className={classes.contentSubTitle}>Relationship</Typography>
                                                                    </Grid>
                                                                    <Grid item xs={6}>
                                                                        <Typography variant="subtitle1" className={classes.contentSubTitleValue}>{contact.relationship}</Typography>
                                                                    </Grid>
                                                                    <Grid item xs={6}>
                                                                        <Typography variant="subtitle1" className={classes.contentSubTitle}>Phone</Typography>
                                                                    </Grid>
                                                                    <Grid item xs={6}>
                                                                        <Typography variant="subtitle1" className={classes.contentSubTitleValue}>{contact.phone}</Typography>
                                                                    </Grid>
                                                                    {
                                                                        contactsArray.length > 0 && <div style={{ width: '100%', border: '1px dashed #cdcdcd' }}></div>
                                                                    }
                                                                </Grid>
                                                            })
                                                        } 
                                                    </Grid>
                                                </Grid>
                                            </CustomPaper>
                                        </Grid>
                                    </Grid>
                                }
                                {
                                    isTabs === 1 && <Grid container spacing={4}>
                                    <Grid item xs>
                                        <CustomPaper>
                                            <Grid container direction="column" style={{maxHeight: '335px', height: '335px'}}>
                                                <Grid container>
                                                    <Grid item xs={12}>
                                                        <Typography variant="h6" className={classes.bold} gutterBottom>{SALARY_TITLE_LABEL.SALARY}</Typography>
                                                    </Grid>
                                                    <Grid item xs={6}>
                                                        <Typography variant="subtitle1" className={classes.contentSubTitle}>{SALARY_LABEL.EARNINGS.BASIC_RATE}</Typography>
                                                    </Grid>
                                                    <Grid item xs={6}>
                                                        <Typography variant="subtitle1" className={classes.contentSubTitleValue}>P{SALARY_VALUE.earnings.basicRate.toFixed(2)}</Typography>
                                                    </Grid>
                                                    <Grid item xs={6}>
                                                        <Typography variant="subtitle1" className={classes.contentSubTitle}>{SALARY_LABEL.EARNINGS.OT_RATE}</Typography>
                                                    </Grid>
                                                    <Grid item xs={6}>
                                                        <Typography variant="subtitle1" className={classes.contentSubTitleValue}>P{SALARY_VALUE.earnings.otRate.toFixed(2)}</Typography>
                                                    </Grid>
                                                    <Grid item xs={6}>
                                                        <Typography variant="subtitle1" className={classes.contentSubTitle}>{SALARY_LABEL.EARNINGS.ALLOWANCE}</Typography>
                                                    </Grid>
                                                    <Grid item xs={6}>
                                                        <Typography variant="subtitle1" className={classes.contentSubTitleValue}>P{SALARY_VALUE.earnings.allowance.toFixed(2)}</Typography>
                                                    </Grid>
                                                    <Grid item xs={6}>
                                                        <Typography variant="subtitle1" className={classes.contentSubTitle}>{SALARY_LABEL.EARNINGS.NIGHT_DIFF}</Typography>
                                                    </Grid>
                                                    <Grid item xs={6}>
                                                        <Typography variant="subtitle1" className={classes.contentSubTitleValue}>P{SALARY_VALUE.earnings.nightDiff.toFixed(2)}</Typography>
                                                    </Grid>
                                                </Grid>
                                            </Grid>
                                        </CustomPaper>
                                    </Grid>
                                    <Grid item xs>
                                        <CustomPaper>
                                            <Grid container direction="column" style={{maxHeight: '335px', height: '335px'}}>
                                                <Grid container>
                                                    <Grid item xs={12}>
                                                        <Typography variant="h6" className={classes.bold} gutterBottom>{SALARY_TITLE_LABEL.DEDUCTIONS}</Typography>
                                                    </Grid>
                                                    <Grid item xs={6}>
                                                        <Typography variant="subtitle1" className={classes.contentSubTitle}>{SALARY_LABEL.DEDUCTIONS.SSS}</Typography>
                                                    </Grid>
                                                    <Grid item xs={6}>
                                                        <Typography variant="subtitle1" className={classes.contentSubTitleValue}>P{SALARY_VALUE.deductions.pagIbig.toFixed(2)}</Typography>
                                                    </Grid>
                                                    <Grid item xs={6}>
                                                        <Typography variant="subtitle1" className={classes.contentSubTitle}>{SALARY_LABEL.DEDUCTIONS.PHIL_HEALTH}</Typography>
                                                    </Grid>
                                                    <Grid item xs={6}>
                                                        <Typography variant="subtitle1" className={classes.contentSubTitleValue}>P{SALARY_VALUE.deductions.philHealth.toFixed(2)}</Typography>
                                                    </Grid>
                                                    <Grid item xs={6}>
                                                        <Typography variant="subtitle1" className={classes.contentSubTitle}>{SALARY_LABEL.DEDUCTIONS.HDMF}</Typography>
                                                    </Grid>
                                                    <Grid item xs={6}>
                                                        <Typography variant="subtitle1" className={classes.contentSubTitleValue}>P{SALARY_VALUE.deductions.hdmfMPL.toFixed(2)}</Typography>
                                                    </Grid>
                                                    <Grid item xs={6}>
                                                        <Typography variant="subtitle1" className={classes.contentSubTitle}>{SALARY_LABEL.DEDUCTIONS.CASH_BOND}</Typography>
                                                    </Grid>
                                                    <Grid item xs={6}>
                                                        <Typography variant="subtitle1" className={classes.contentSubTitleValue}>P{SALARY_VALUE.deductions.cashBond.toFixed(2)}</Typography>
                                                    </Grid>
                                                    <Grid item xs={6}>
                                                        <Typography variant="subtitle1" className={classes.contentSubTitle}>{SALARY_LABEL.DEDUCTIONS.DEATH_CONTRI}</Typography>
                                                    </Grid>
                                                    <Grid item xs={6}>
                                                        <Typography variant="subtitle1" className={classes.contentSubTitleValue}>P{SALARY_VALUE.deductions.deathContri.toFixed(2)}</Typography>
                                                    </Grid>
                                                    <Grid item xs={6}>
                                                        <Typography variant="subtitle1" className={classes.contentSubTitle}>{SALARY_LABEL.DEDUCTIONS.SSS_LOAN}</Typography>
                                                    </Grid>
                                                    <Grid item xs={6}>
                                                        <Typography variant="subtitle1" className={classes.contentSubTitleValue}>P{SALARY_VALUE.deductions.sssSalaryLoan.toFixed(2)}</Typography>
                                                    </Grid>
                                                </Grid>
                                            </Grid>
                                        </CustomPaper>
                                    </Grid>
                                </Grid>
                                //     isTabs === 1 && <CustomPaper>
                                //     <Grid item xs={12}>
                                //             <Grid container>
                                //                 <Grid item xs={6} style={{ border: '1px solid rgb(237, 237, 237)', padding: '2rem' }}>
                                //                     <Grid container direction="column">
                                //                         <TitleContainerComponent label="Earnings" minHeight={'15px'} fontWeight={700}/>
                                //                         <Grid item xs={12}>
                                //                             <Grid container style={{padding:'1rem'}}>
                                //                                 <Grid item xs={12} style={{margin: '.5rem 0rem'}} container>
                                //                                     <Grid item xs={4} style={{alignSelf: 'center'}}>
                                //                                         <Typography>Basic Rate</Typography>
                                //                                     </Grid>
                                //                                     <Grid item xs={8}>
                                //                                         <div style={{ padding: '5px', borderRadius: '5px', background: bgColor }}>
                                //                                             <Typography>{EARNINGS.basicRate + ' PHP'}</Typography>
                                //                                         </div>
                                //                                     </Grid>
                                //                                 </Grid>
                                //                                 <Grid item xs={12} style={{margin: '.5rem 0rem'}} container>
                                //                                     <Grid item xs={4} style={{alignSelf: 'center'}}>
                                //                                         <Typography>OT Rate</Typography>
                                //                                     </Grid>
                                //                                     <Grid item xs={8}>
                                //                                         <div style={{ padding: '5px', borderRadius: '5px', background: bgColor }}>
                                //                                             <Typography>{EARNINGS.otRate + '%'}</Typography>
                                //                                         </div>
                                //                                     </Grid>
                                //                                 </Grid>
                                //                                 <Grid item xs={12} style={{margin: '.5rem 0rem'}} container>
                                //                                     <Grid item xs={4} style={{alignSelf: 'center'}}>
                                //                                         <Typography>Allowance</Typography>
                                //                                     </Grid>
                                //                                     <Grid item xs={8}>
                                //                                         <div style={{ padding: '5px', borderRadius: '5px', background: bgColor }}>
                                //                                             <Typography>{EARNINGS.allowance + ' PHP'}</Typography>
                                //                                         </div>
                                //                                     </Grid>
                                //                                 </Grid>
                                //                                 <Grid item xs={12} style={{margin: '.5rem 0rem'}} container>
                                //                                     <Grid item xs={4} style={{alignSelf: 'center'}}>
                                //                                         <Typography>Night Differential</Typography>
                                //                                     </Grid>
                                //                                     <Grid item xs={8}>
                                //                                         <div style={{ padding: '5px', borderRadius: '5px', background: bgColor }}>
                                //                                             <Typography>{EARNINGS.nightDifferential + '%'}</Typography>
                                //                                         </div>
                                //                                     </Grid>
                                //                                 </Grid>
                                //                             </Grid>
                                //                         </Grid>
                                //                     </Grid>
                                //                 </Grid>  
                                //                 <Grid item xs={6} style={{ border: '1px solid rgb(237, 237, 237)', padding: '2rem' }}>
                                //                     <Grid container>
                                //                         <TitleContainerComponent label="Deduction" minHeight={'15px'} fontWeight={700}/>
                                //                         <Grid item xs={12}>
                                //                             <Grid container style={{padding:'1rem'}}>
                                //                                 <Grid item xs={12} style={{margin: '.5rem 0rem'}} container>
                                //                                     <Grid item xs={4} style={{alignSelf: 'center'}}>
                                //                                         <Typography>SSS</Typography>
                                //                                     </Grid>
                                //                                     <Grid item xs={8}>
                                //                                         <div style={{ padding: '5px', borderRadius: '5px', background: bgColor }}>
                                //                                             <Typography>{DEDUCTIONS.sss + ' PHP'}</Typography>
                                //                                         </div>
                                //                                     </Grid>
                                //                                 </Grid>
                                //                                 <Grid item xs={12} style={{margin: '.5rem 0rem'}} container>
                                //                                     <Grid item xs={4} style={{alignSelf: 'center'}}>
                                //                                         <Typography>Pag-Ibig</Typography>
                                //                                     </Grid>
                                //                                     <Grid item xs={8}>
                                //                                         <div style={{ padding: '5px', borderRadius: '5px', background: bgColor }}>
                                //                                             <Typography>{DEDUCTIONS.pagIbig + ' PHP'}</Typography>
                                //                                         </div>
                                //                                     </Grid>
                                //                                 </Grid>
                                //                                 <Grid item xs={12} style={{margin: '.5rem 0rem'}} container>
                                //                                     <Grid item xs={4} style={{alignSelf: 'center'}}>
                                //                                         <Typography>PhilHealth</Typography>
                                //                                     </Grid>
                                //                                     <Grid item xs={8}>
                                //                                         <div style={{ padding: '5px', borderRadius: '5px', background: bgColor }}>
                                //                                             <Typography>{DEDUCTIONS.philHealth + ' PHP'}</Typography>
                                //                                         </div>
                                //                                     </Grid>
                                //                                 </Grid>
                                //                                 <Grid item xs={12} style={{margin: '.5rem 0rem'}} container>
                                //                                     <Grid item xs={4} style={{alignSelf: 'center'}}>
                                //                                         <Typography>HDMF MPL</Typography>
                                //                                     </Grid>
                                //                                     <Grid item xs={8}>
                                //                                         <div style={{ padding: '5px', borderRadius: '5px', background: bgColor }}>
                                //                                             <Typography>{DEDUCTIONS.hdmf + ' PHP'}</Typography>
                                //                                         </div>
                                //                                     </Grid>
                                //                                 </Grid>
                                //                                 <Grid item xs={12} style={{margin: '.5rem 0rem'}} container>
                                //                                     <Grid item xs={4} style={{alignSelf: 'center'}}>
                                //                                         <Typography>Cash Bond</Typography>
                                //                                     </Grid>
                                //                                     <Grid item xs={8}>
                                //                                         <div style={{ padding: '5px', borderRadius: '5px', background: bgColor }}>
                                //                                             <Typography>{DEDUCTIONS.cashBond + ' PHP'}</Typography>
                                //                                         </div>
                                //                                     </Grid>
                                //                                 </Grid>
                                //                                 <Grid item xs={12} style={{margin: '.5rem 0rem'}} container>
                                //                                     <Grid item xs={4} style={{alignSelf: 'center'}}>
                                //                                         <Typography>Death Contribution</Typography>
                                //                                     </Grid>
                                //                                     <Grid item xs={8}>
                                //                                         <div style={{ padding: '5px', borderRadius: '5px', background: bgColor }}>
                                //                                             <Typography>{DEDUCTIONS.deathContribution + ' PHP'}</Typography>
                                //                                         </div>
                                //                                     </Grid>
                                //                                 </Grid>
                                //                                 <Grid item xs={12} style={{margin: '.5rem 0rem'}} container>
                                //                                     <Grid item xs={4} style={{alignSelf: 'center'}}>
                                //                                         <Typography>SSS Salary Loan</Typography>
                                //                                     </Grid>
                                //                                     <Grid item xs={8}>
                                //                                         <div style={{ padding: '5px', borderRadius: '5px', background: bgColor }}>
                                //                                             <Typography>{DEDUCTIONS.sssSalaryLoan + ' PHP'}</Typography>
                                //                                         </div>
                                //                                     </Grid>
                                //                                 </Grid>
                                //                             </Grid>
                                //                         </Grid>
                                //                     </Grid>
                                //                 </Grid>  
                                //             </Grid>
                                //         </Grid>
                                // </CustomPaper>
                                }
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </CustomContainer>
        </div>
    )
}

export default EmployeeAccountsComponent;