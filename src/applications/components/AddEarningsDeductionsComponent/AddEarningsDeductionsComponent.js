import React from 'react';
import {
    Grid,
    Typography,
} from '@material-ui/core';

import CustomTextField from '../../Shared/CustomTextField/CustomTextField';
import {makeStyles} from '@material-ui/core/styles';
import TitleContainerComponent from '../TitleContainerComponent/TitleContainerComponent';

const useStyles = makeStyles((theme) => ({
    modalContainer: {
        width: '1000px'
    }
}));

export default function AddEarningsDeductions() {
    const classes = useStyles();
    return (
        <div>
            <Grid container spacing={2} style={{marginTop:'1rem'}}>
                <Grid item xs={12}>
                    <Grid container spacing={4}>
                        <Grid item xs={6}>
                            <Grid container>
                                <Grid item xs align="center">
                                    <TitleContainerComponent label="EARNINGS" fontWeight={700} minHeight={'15px'}/>
                                </Grid>
                            </Grid>
                            <CustomTextField label="Basic Rate" size="small" inlineLabel={true}/>
                            <CustomTextField label="OT Rate" size="small" inlineLabel={true}/>
                            <CustomTextField label="Allowance" size="small" inlineLabel={true}/>
                            <CustomTextField label="Night Differential" size="small" inlineLabel={true}/>
                        </Grid>
                        <Grid item xs={6}>
                            <Grid container>
                                <Grid item xs align="center">
                                    <TitleContainerComponent label="DEDUCTIONS" fontWeight={700} minHeight={'15px'}/>
                                </Grid>
                            </Grid>
                            <CustomTextField label="SSS Contribution" size="small" inlineLabel={true}/>
                            <CustomTextField label="Pag-ibig Contribution" size="small" inlineLabel={true}/>
                            <CustomTextField label="Philhealth Contribution" size="small" inlineLabel={true}/>
                            <CustomTextField label="Cash bond" size="small" inlineLabel={true}/> 
                            <CustomTextField label="SSS Loan" size="small" inlineLabel={true}/>
                            
                            <CustomTextField label="Pag-ibig Loan" size="small" inlineLabel={true}/>
                            <CustomTextField label="Paraphernalias" size="small" inlineLabel={true}/>
                            <CustomTextField label="Death Contribution" size="small" inlineLabel={true}/>
                            <CustomTextField label="Security License" size="small" inlineLabel={true}/>
                            <CustomTextField label="Training Expense" size="small" inlineLabel={true}/>
                            <CustomTextField label="Barracks Fee" size="small" inlineLabel={true}/>
                            <CustomTextField label="Insurance Fee" size="small" inlineLabel={true}/>
                            <CustomTextField label="FAS Loss" size="small" inlineLabel={true}/>
                            <CustomTextField label="Fines and Penalties" size="small" inlineLabel={true}/>
                            <CustomTextField label="NTC License" size="small" inlineLabel={true}/>
                            <CustomTextField label="Cash Advance" size="small" inlineLabel={true}/>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        </div>
    )
}