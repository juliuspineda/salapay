import React from 'react';
import {
  ResponsiveContainer, PieChart, Pie, Legend, Cell, Tooltip
} from 'recharts';

function PieChartComponent(props) {
    const {
        data,
        // label,
        // startAngle,
        // endAngle,
        // cx,
        // cy,
        // innerRadius,
        // outerRadius,
        // fill,
        width,
        height,
        hasToolTip,
    } = props;

    const COLORS = ['#0088FE', '#00C49F', '#FFBB28', '#FF8042'];

    return (
        <div style={{ height: height, width: width }}>
            <ResponsiveContainer>
                <PieChart>
                    <Pie 
                        dataKey="value" 
                        data={data} 
                        fill="8884d8"
                        label    
                    >
                        {
                            data && data.map((entry, index) => <Cell fill={COLORS[index % COLORS.length]}/>)
                        }
                    </Pie>
                { hasToolTip && <Tooltip /> }
                </PieChart>
            </ResponsiveContainer>
        </div>
    );
}

// PieChartComponent.defaultProps = {
//     data: {}
// }

export default PieChartComponent;
