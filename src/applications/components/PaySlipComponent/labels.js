export const PAYROLL_LABELS = {
    TITLE_LABEL: 'PAYROLL COVERED',
    SUBTITLE_LABEL: 'SALAPAY',
    EMPLOYEE_DETAIL_LABEL: 'EMPLOYEE DETAILS',
    EMPLOYEE_ID_LABEL: 'EMPLOYEE ID',
    EMPLOYEE_NAME_LABEL: 'EMPLOYEE NAME',
    DATE_LABEL: 'DATE'
}

export const PAYROLL_SUB_LABELS = {
    EARNINGS: 'EARNINGS',
    DEDUCTIONS: 'DEDUCTIONS',
    LOAN_BALANCES: 'LOAN BALANCES',
    GROSS_PAY: 'GROSS PAY',
    TOTAL_DEDUCTIONS:  'TOTAL DEDUCTIONS',
    NET_PAY: 'NET PAY',
    REMARKS: 'NOTES/REMARKS'
}

export const EARNING_LABELS = {
    TWD: 'Total Working Days',
    TOP: 'Total Overtime Pay',
    THP: 'Total Holiday Pay',
    TND: 'Total Night Differential',
    APC: 'Adjustment from Previous Cut-off',
    ALLOWANCE: 'Allowance',
    PDS: 'Per Dispatchment Summary',
    NWD: 'No. of Working Days <rate>',
    OVERTIME: 'Overtime <rate>',
    HOLIDAYS: 'Holidays <rate>',
    ND: 'Night Differential <rate>'
}

export const DEDUCTIONS_LABELS = {
    SSS_LABEL: 'SSS Contribution',
    PAGIBIG_LABEL: 'Pag-ibig Contribution',
    PHIL_LABEL: 'Philhealth Contribution',
    CASH_BOND: 'Cash Bond',
    SSS_LOAN: 'SSS Loan',
    PAGIBIG_LOAN: 'Pag-ibig Loan',
    PARAPHERNALIAS: 'Paraphernalias',
    DEATH_CONTRIBUTION: 'Death Contribution',
    SECURITY_LICENSE: 'Security License',
    TRAINING_EXPENSE: 'Training Expense',
    BARRACKS_FEE: 'Barracks Fee',
    INSURANCE_FEE: 'Insurance Fee',
    FAS_LOSS: 'FAS Loss',
    FINES_AND_PENALTIES: 'Fines and Penalties',
    NTC_LICENSE: 'NTC License',
    CASH_ADVANCE: 'Cash Advance',
    OTHERS: 'Others'
}

export const LOAN_BALANCES_LABELS = {
    CAB: 'Cash Advance Balance',
    SSS_LOAN: 'SSS Loan',
    PAGIBIG_LOAN: 'Pag-ibig Loan'
}

export const TAX_LABELS = {
    TIY: 'Taxable Income YTD',
    TDY: 'Tax Deductions YTD',
    SCY: 'SSS Contribution YTD',
    PCY: 'Pag-ibig Contribution YTD',
    PY: 'Philhealth YTD'
}