import React from 'react';
import { 
    PAYROLL_LABELS,
    PAYROLL_SUB_LABELS,
    EARNING_LABELS,
    DEDUCTIONS_LABELS,
    LOAN_BALANCES_LABELS,
    TAX_LABELS 
} from './labels';

import styles from './payslip.module.scss';

const INITIAL_PAYROLL_VALUE = {
    value: 'P0.00',
    employeeID: '20027358',
    employeeName: 'CHRISTIAN GASPAR',
    payrollDate: new Date().toISOString().split('T')[0]
}

function PaySlipComponent(props) {
    //const { /* */ } = props;

    return (
        <div className={styles.payslipContainer}>
            <div className={styles.headerContainer}>
                <span>
                    <h1 className={styles.title}>
                        {PAYROLL_LABELS.TITLE_LABEL}
                    </h1>
                </span>
                <span>
                    <p className={styles.textContainer}>
                        {PAYROLL_LABELS.SUBTITLE_LABEL}
                    </p>
                </span>
            </div>
            <div className={styles.detailsContainer}>
                <div className={styles.detailsSubContainer}>
                    <div>
                        <h2>{PAYROLL_LABELS.EMPLOYEE_DETAIL_LABEL}</h2>
                    </div>
                    <div>
                        <div className={styles.flexContainer}>
                            <span>
                                <p className={styles.textContainer}>
                                    {PAYROLL_LABELS.DATE_LABEL}: 
                                    <span className={styles.textValue}>{INITIAL_PAYROLL_VALUE.payrollDate}</span>
                                </p>
                            </span>
                        </div>
                    </div>
                </div>
                <div className={styles.detailsSubContainer}>
                    <div>
                        <div className={styles.flexContainer}>
                            <span>
                                <p className={styles.textContainer}>
                                    {PAYROLL_LABELS.EMPLOYEE_ID_LABEL}: 
                                    <span className={styles.textValue}>{INITIAL_PAYROLL_VALUE.employeeID}</span>
                                </p>
                            </span>
                        </div>
                        <div className={styles.flexContainer}>
                            <span>
                                <p className={styles.textContainer}>
                                    {PAYROLL_LABELS.EMPLOYEE_NAME_LABEL}: 
                                    <span className={styles.textValue}>{INITIAL_PAYROLL_VALUE.employeeName}</span>
                                </p>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <div className={styles.contentContainer}>
                <div className={styles.column}>
                    <table>
                        <tr>
                            <th colSpan={3}>{PAYROLL_SUB_LABELS.EARNINGS}</th>
                        </tr>
                        <tr>
                            <td>{EARNING_LABELS.TWD}</td>
                            <td></td>
                            <td className={styles.align} style={{width: '30%'}}>{INITIAL_PAYROLL_VALUE.value}</td>
                        </tr>
                        <tr>
                            <td>{EARNING_LABELS.TOP}</td>
                            <td></td>
                            <td className={styles.align} style={{width: '30%'}}>{INITIAL_PAYROLL_VALUE.value}</td>
                        </tr>
                        <tr>
                            <td>{EARNING_LABELS.THP}</td>
                            <td></td>
                            <td className={styles.align} style={{width: '30%'}}>{INITIAL_PAYROLL_VALUE.value}</td>
                        </tr>
                        <tr>
                            <td>{EARNING_LABELS.TND}</td>
                            <td></td>
                            <td className={styles.align} style={{width: '30%'}}>{INITIAL_PAYROLL_VALUE.value}</td>
                        </tr>
                        <tr>
                            <td>{EARNING_LABELS.APC}</td>
                            <td></td>
                            <td className={styles.align} style={{width: '30%'}}>{INITIAL_PAYROLL_VALUE.value}</td>
                        </tr>
                        <tr>
                            <td>{EARNING_LABELS.ALLOWANCE}</td>
                            <td></td>
                            <td className={styles.align} style={{width: '30%'}}>{INITIAL_PAYROLL_VALUE.value}</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>{'Per Detachment Summary'}</td>
                            <td className={styles.align}>{'No. of Days'}</td>
                            <td style={{width: '30%'}}></td>
                        </tr>
                        <tr>
                            <td colSpan={3}>{'<Detachment 1>'}</td>
                        </tr>
                        <tr>
                            <td>{EARNING_LABELS.NWD}</td>
                            <td className={styles.alignCenter}>14</td>
                            <td className={styles.align} style={{width: '30%'}}>{INITIAL_PAYROLL_VALUE.value}</td>
                        </tr>
                        <tr>
                            <td>{EARNING_LABELS.OVERTIME}</td>
                            <td className={styles.alignCenter}>2</td>
                            <td className={styles.align} style={{width: '30%'}}>{INITIAL_PAYROLL_VALUE.value}</td>
                        </tr>
                        <tr>
                            <td>{EARNING_LABELS.HOLIDAYS}</td>
                            <td className={styles.alignCenter}>2</td>
                            <td className={styles.align} style={{width: '30%'}}>{INITIAL_PAYROLL_VALUE.value}</td>
                        </tr>
                        <tr>
                            <td>{EARNING_LABELS.ND}</td>
                            <td className={styles.alignCenter}>2</td>
                            <td className={styles.align} style={{width: '30%'}}>{INITIAL_PAYROLL_VALUE.value}</td>
                        </tr>
                        <tr>
                            <td colSpan={3}>{'<Detachment 1>'}</td>
                        </tr>
                        <tr>
                            <td>{EARNING_LABELS.NWD}</td>
                            <td className={styles.alignCenter}>14</td>
                            <td className={styles.align} style={{width: '30%'}}>{INITIAL_PAYROLL_VALUE.value}</td>
                        </tr>
                        <tr>
                            <td>{EARNING_LABELS.OVERTIME}</td>
                            <td className={styles.alignCenter}>2</td>
                            <td className={styles.align} style={{width: '30%'}}>{INITIAL_PAYROLL_VALUE.value}</td>
                        </tr>
                        <tr>
                            <td>{EARNING_LABELS.HOLIDAYS}</td>
                            <td className={styles.alignCenter}>2</td>
                            <td className={styles.align} style={{width: '30%'}}>{INITIAL_PAYROLL_VALUE.value}</td>
                        </tr>
                        <tr>
                            <td>{EARNING_LABELS.ND}</td>
                            <td className={styles.alignCenter}>2</td>
                            <td className={styles.align} style={{width: '30%'}}>{INITIAL_PAYROLL_VALUE.value}</td>
                        </tr>
                    </table>
                </div>
                <div className={styles.column}>
                    <table>
                        <tr>
                            <th colSpan={2}>{PAYROLL_SUB_LABELS.DEDUCTIONS}</th>
                        </tr>
                        <tr>
                            <td>{DEDUCTIONS_LABELS.SSS_LABEL}</td>
                            <td className={styles.align}>{INITIAL_PAYROLL_VALUE.value}</td>
                        </tr>
                        <tr>
                            <td>{DEDUCTIONS_LABELS.PAGIBIG_LABEL}</td>
                            <td className={styles.align}>{INITIAL_PAYROLL_VALUE.value}</td>
                        </tr>
                        <tr>
                            <td>{DEDUCTIONS_LABELS.PHIL_LABEL}</td>
                            <td className={styles.align}>{INITIAL_PAYROLL_VALUE.value}</td>
                        </tr>
                        <tr>
                            <td>{DEDUCTIONS_LABELS.CASH_BOND}</td>
                            <td className={styles.align}>{INITIAL_PAYROLL_VALUE.value}</td>
                        </tr>
                        <tr>
                            <td>{DEDUCTIONS_LABELS.SSS_LOAN}</td>
                            <td className={styles.align}>{INITIAL_PAYROLL_VALUE.value}</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>{DEDUCTIONS_LABELS.PAGIBIG_LOAN}</td>
                            <td className={styles.align}>{INITIAL_PAYROLL_VALUE.value}</td>
                        </tr>
                        <tr>
                            <td>{DEDUCTIONS_LABELS.PARAPHERNALIAS}</td>
                            <td className={styles.align}>{INITIAL_PAYROLL_VALUE.value}</td>
                        </tr>
                        <tr>
                            <td>{DEDUCTIONS_LABELS.DEATH_CONTRIBUTION}</td>
                            <td className={styles.align}>{INITIAL_PAYROLL_VALUE.value}</td>
                        </tr>
                        <tr>
                            <td>{DEDUCTIONS_LABELS.SECURITY_LICENSE}</td>
                            <td className={styles.align}>{INITIAL_PAYROLL_VALUE.value}</td>
                        </tr>
                        <tr>
                            <td>{DEDUCTIONS_LABELS.TRAINING_EXPENSE}</td>
                            <td className={styles.align}>{INITIAL_PAYROLL_VALUE.value}</td>
                        </tr>
                        <tr>
                            <td>{DEDUCTIONS_LABELS.BARRACKS_FEE}</td>
                            <td className={styles.align}>{INITIAL_PAYROLL_VALUE.value}</td>
                        </tr>
                        <tr>
                            <td>{DEDUCTIONS_LABELS.INSURANCE_FEE}</td>
                            <td className={styles.align}>{INITIAL_PAYROLL_VALUE.value}</td>
                        </tr>
                        <tr>
                            <td>{DEDUCTIONS_LABELS.FAS_LOSS}</td>
                            <td className={styles.align}>{INITIAL_PAYROLL_VALUE.value}</td>
                        </tr>
                        <tr>
                            <td>{DEDUCTIONS_LABELS.FINES_AND_PENALTIES}</td>
                            <td className={styles.align}>{INITIAL_PAYROLL_VALUE.value}</td>
                        </tr>
                        <tr>
                            <td>{DEDUCTIONS_LABELS.NTC_LICENSE}</td>
                            <td className={styles.align}>{INITIAL_PAYROLL_VALUE.value}</td>
                        </tr>
                        <tr>
                            <td>{DEDUCTIONS_LABELS.CASH_ADVANCE}</td>
                            <td className={styles.align}>{INITIAL_PAYROLL_VALUE.value}</td>
                        </tr>
                        <tr>
                            <td>{DEDUCTIONS_LABELS.OTHERS}</td>
                            <td className={styles.align}>{INITIAL_PAYROLL_VALUE.value}</td>
                        </tr>
                    </table>
                </div>
                <div className={styles.column}>
                    <div className={styles.loanBalances}>
                        <table>
                            <tr>
                                <th colSpan={2}>{PAYROLL_SUB_LABELS.LOAN_BALANCES}</th>
                            </tr>
                            <tr>
                                <td>{LOAN_BALANCES_LABELS.CAB}</td>
                                <td className={styles.align}>{INITIAL_PAYROLL_VALUE.value}</td>
                            </tr>
                            <tr>
                                <td>{LOAN_BALANCES_LABELS.SSS_LOAN}</td>
                                <td className={styles.align}>{INITIAL_PAYROLL_VALUE.value}</td>
                            </tr>
                            <tr>
                                <td>{LOAN_BALANCES_LABELS.PAGIBIG_LOAN}</td>
                                <td className={styles.align}>{INITIAL_PAYROLL_VALUE.value}</td>
                            </tr>
                            <tr>
                                <td>{TAX_LABELS.TIY}</td>
                                <td className={styles.align}>{INITIAL_PAYROLL_VALUE.value}</td>
                            </tr>
                            <tr>
                                <td>{TAX_LABELS.TDY}</td>
                                <td className={styles.align}>{INITIAL_PAYROLL_VALUE.value}</td>
                            </tr>
                            <tr style={{height: '120px'}}>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>{TAX_LABELS.SCY}</td>
                                <td className={styles.align}>{INITIAL_PAYROLL_VALUE.value}</td>
                            </tr>
                            <tr>
                                <td>{TAX_LABELS.PCY}</td>
                                <td className={styles.align}>{INITIAL_PAYROLL_VALUE.value}</td>
                            </tr>
                            <tr>
                                <td>{TAX_LABELS.PY}</td>
                                <td className={styles.align}>{INITIAL_PAYROLL_VALUE.value}</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div className={styles.contentContainer}>
                <div className={styles.column}>
                    <div className={styles.flexContainer}>
                        <div>
                            <p className={styles.textContainer}>
                                {PAYROLL_SUB_LABELS.GROSS_PAY}: 
                            </p>
                        </div>
                        <div>
                            <p className={styles.textContainer}>
                                <span className={styles.textValue}>{INITIAL_PAYROLL_VALUE.value}</span>
                            </p>
                        </div>
                    </div>
                </div>
                <div className={styles.column}>
                    <div className={styles.flexContainer}>
                        <div>
                            <p className={styles.textContainer}>
                                {PAYROLL_SUB_LABELS.TOTAL_DEDUCTIONS}: 
                            </p>
                        </div>
                        <div>
                            <p className={styles.textContainer}>
                                <span className={styles.textValue}>{INITIAL_PAYROLL_VALUE.value}</span>
                            </p>
                        </div>
                    </div>
                </div>
                <div className={styles.column}>
                    <div className={styles.flexContainer}>
                        <div>
                            <p className={styles.textContainer}>
                                {PAYROLL_SUB_LABELS.NET_PAY}: 
                            </p>
                        </div>
                        <div>
                            <p className={styles.textContainer}>
                                <span className={styles.textValue}>{INITIAL_PAYROLL_VALUE.value}</span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            {/* <div className={styles.contentContainer}>
                <div className={styles.remarks}>
                    <span>
                        <p className={styles.textContainer}>
                            {PAYROLL_SUB_LABELS.REMARKS}: 
                            <span className={styles.textValue}>{''}</span>
                        </p>
                    </span>
                </div>
            </div> */}
        </div>
    )    
}

export default PaySlipComponent;