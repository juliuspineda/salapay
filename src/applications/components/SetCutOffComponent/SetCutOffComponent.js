import React from 'react';
import {
    Grid,
    TextField
} from '@material-ui/core';

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: 200,
  },
}));

export default function SetCutOffComponent() {
    const classes = useStyles();
    return (
        <React.Fragment>
            <Grid container spacing={2}>
                <Grid item xs={6}>
                <form className={classes.container} noValidate>
                <TextField
                    id="date"
                    label="Start Date"
                    type="date"
                    defaultValue="2017-05-24"
                    className={classes.textField}
                    InputLabelProps={{
                    shrink: true,
                    }}
                />
                </form>
                </Grid>
                <Grid item xs={6}>
                <form className={classes.container} noValidate>
                <TextField
                    id="date"
                    label="End Date"
                    type="date"
                    defaultValue="2017-05-24"
                    className={classes.textField}
                    InputLabelProps={{
                    shrink: true,
                    }}
                />
                </form>
                </Grid>
            </Grid>
        </React.Fragment>
    )
}