import React, { useState, forwardRef } from 'react';
import MaterialTable from 'material-table';

import { Link } from "react-router-dom";

import { Paper } from '@material-ui/core';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import AddBox from '@material-ui/icons/AddBox';
import PersonOutlineIcon from '@material-ui/icons/PersonOutline';
import ArrowDownward from '@material-ui/icons/ArrowDownward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';

import EmployeeAccountsComponent from '../../components/EmployeeAccountsComponent/EmployeeAccountsComponent';
import CustomDialog from '../../Shared/CustomDialog/CustomDialog';
import ViewDispatchDetailsComponent from '../../components/ViewDispatchDetailsComponent/ViewDispatchDetailsComponent';
import CustomPaper from '../../Shared/CustomPaper/CustomPaper';

import Avatar from '@material-ui/core/Avatar'

function TableComponent(props) {
    const {
        columns,
        data,
        setData,
        title = '',
    } = props;

    const [selectedRow, setSelectedRow] = useState(null);
    const [openModal, setOpenModal] = useState(false);

    const tableIcons = {
        Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
        Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
        Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
        Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
        DetailPanel: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
        Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
        Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
        Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
        FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
        LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
        NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
        PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
        ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
        Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
        SortArrow: forwardRef((props, ref) => <ArrowDownward {...props} ref={ref} />),
        ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
        ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />)
    };
    
    const handleClose = () => {
        setOpenModal(false);
    };

    const children = () => {

    }
    
      return (
        <CustomPaper noPadding>
            <MaterialTable
                icons={tableIcons}
                title={title}
                columns={
                    [
                        ...columns.map(data => {
                            return {
                                field: data.field,
                                title: data.title,
                                render: rowData => 
                                    (<div style={{ 
                                            minWidth: data.field === 'employeeName' ? '160px' :
                                            data.field === 'dispatchmentId' ? '0px' : 
                                            data.field === 'dispatchmentName' ? '175px' : 
                                            data.field === 'wageRate' ? '120px' :
                                            data.field === 'daysOfWork' ? '120px' : 
                                            data.field === 'basicPay' ? '120px' : 
                                            data.field === 'ndDays' ? '100px' :
                                            data.field === 'ndPay' ? '100px' : 
                                            data.field === 'grossPay' ? '120px' : 
                                            data.field === 'pagIbigDispatchment' ? '120px' : 
                                            data.field === 'hmdfMpl' ? '120px' : 
                                            data.field === 'cashBond' ? '120px' : 
                                            data.field === 'arAndOe' ? '120px' : 
                                            data.field === 'deathContribution' ? '180px' :
                                            data.field === 'totalDeductions' ? '160px' : 
                                            data.field === 'netPay' ? '120px' : 
                                            data.field === 'srNo' ? '100px' :
                                            data.field === 'payableLocation' ? '160px' : 
                                            data.field === 'acNo' ? '120px' : 
                                            data.field === 'instrumentRefNo' ? '170px' : 
                                            data.field === 'customerRefNo' ? '170px' : 
                                            data.field === 'payeeACNo' ? '150px' : 
                                            data.field === 'payeeName' ? '130px' : 
                                            data.field === 'intrumentNo' ? '150px' : 
                                            data.field === 'crBranchCode' ? '160px' : 
                                            data.field === 'valueDate' ? '120px' : 
                                            data.field === 'debitDate' ? '120px' : 
                                            data.field === 'instrumentAmount' ? '170px' : 
                                            data.field === 'issuingBranch' ? '150px' : 
                                            data.field === 'agingDays' ? '140px' : 
                                            data.field === 'liquidationDate' ? '160px' : 
                                            data.field === 'txnStatus' ? '150px' : 
                                            data.field === 'companyName' ? '150px' :
                                            null, 
                                            padding: window.location.pathname === '/dispatchments' ? '6px 13px' : window.location.pathname === '/reports' ? '6px 13px' : window.location.pathname === '/clients' ? '9px 13px' : '0px 13px'
                                        }}>
                                            {/* {
                                                data.field === 'photo' && <img src={'https://avatars0.githubusercontent.com/u/7895451?s=460&v=4'} style={{ height: '35px', width: '35px', borderRadius: '50%' }} />
                                            } */}
                                            {
                                                data.field === 'photo' && (
                                                    <Avatar style={{ height: '35px', width: '35px' }}/>
                                                  )
                                            }
                                            {
                                                rowData[data.field]
                                            }
                                        </div>
                                )
                            }
                        })
                    ]
                }
                data={data}
                components={{
                    Container: props => <Paper {...props} elevation={0}/>
                }}
                actions={
                    window.location.pathname === '/dispatchments' ? 
                    [
                        {
                            icon: () => <PersonOutlineIcon />,
                            tooltip: 'View',
                            //onClick: (event, rowData) => alert("You saved " + rowData.name)
                            onClick: (event, rowData) => setOpenModal(true)
                        }
                    ] : ''
                }
                onRowClick={ (event, rowData) => {
                    //setSelectedRow(rowData.tableData.id);
                    if(window.location.pathname === '/employees') {
                        props.history.push({
                            pathname: "/employee/account",
                            state: {
                                data: data[rowData.tableData.id]
                            }
                        });
                    } else if(window.location.pathname === '/payroll') {
                        props.history.push('/payroll/accounts')
                    }
                } }
                options={{ 
                    //tableLayout: 'fixed',
                    // fixedColumns: {
                    // left: 0,
                    // right: 1
                    // },
                    //width: 100,
                    doubleHorizontalScroll: true,
                    paginationType: 'stepped',
                    loadingType: 'overlay',
                    exportButton: window.location.pathname === '/reports' ? true : false,
                    actionsColumnIndex: -1, 
                    headerStyle: { fontSize: '12px', fontWeight: '700', padding: '15px 30px', zIndex: 0 }, 
                    rowStyle: rowData => ({
                        //backgroundColor: (selectedRow === rowData.tableData.id) ? '#EEE' : '#FFF',
                        fontSize: '12px'
                    }),
                    addRowPosition: 'first',
                    defaultSort: 'asc'
                }}
                editable={{
                    onRowAdd: newData =>
                    new Promise((resolve, reject) => {
                        setTimeout(() => {
                        setData([...data, newData]);
                        
                        resolve();
                        }, 1000)
                    }),
                    onRowUpdate: (newData, oldData) =>
                    new Promise((resolve, reject) => {
                        setTimeout(() => {
                        const dataUpdate = [...data];
                        const index = oldData.tableData.id;
                        dataUpdate[index] = newData;
                        setData([...dataUpdate]);
            
                        resolve();
                        }, 1000)
                    }),
                    onRowDelete: oldData =>
                    new Promise((resolve, reject) => {
                        setTimeout(() => {
                        const dataDelete = [...data];
                        const index = oldData.tableData.id;
                        dataDelete.splice(index, 1);
                        setData([...dataDelete]);
            
                        resolve();
                        }, 1000)
                    }),
                }}
                />
            <CustomDialog 
                open={openModal} 
                onClose={handleClose}
                title='Dispatchment Details'
                width={ openModal ? '800px' : '' }
                children={<ViewDispatchDetailsComponent />}
            />
            {/* <Fab color="primary" aria-label="add">
                <AddIcon />
            </Fab> */}
        </CustomPaper>
      )
}

export default TableComponent;