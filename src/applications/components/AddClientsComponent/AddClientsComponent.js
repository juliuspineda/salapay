import React from 'react';
import {
    Grid,
} from '@material-ui/core';

import CustomTextField from '../../Shared/CustomTextField/CustomTextField';

export default function AddClientsComponent() {
    return (
        <React.Fragment>
            <Grid container spacing={2}>
                <Grid item xs={6}>
                    <CustomTextField label="First Name"/>
                </Grid>
                <Grid item xs={6}>
                    <CustomTextField label="Last Name"/>
                </Grid>
                <Grid item xs={6}>
                    <CustomTextField label="Username"/>
                </Grid>
                <Grid item xs={6}>
                    <CustomTextField label="Email"/>
                </Grid>
                <Grid item xs={6}>
                    <CustomTextField label="Password"/>
                </Grid>
                <Grid item xs={6}>
                    <CustomTextField label="Phone"/>
                </Grid>
                <Grid item xs={6}>
                    <CustomTextField label="Client ID"/>
                </Grid>
                <Grid item xs={6}>
                    <CustomTextField label="Company Name"/>
                </Grid>
            </Grid>
        </React.Fragment>
    )
}