import React from 'react';
import {
    Grid,
} from '@material-ui/core';

import AddIcon from '@material-ui/icons/Add';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';

import CustomButton from '../../Shared/CustomButton/CustomButton';

function SetReminderComponent(props) {
    const { options } = props;
    console.log('options', options);
    return(
        <React.Fragment>
            <Grid container>
                <Grid item xs={12}>
                    <CustomButton label="Add" color="primary" icon={<AddIcon />} onClick={options.add} onClose={options.onClose}/>
                </Grid>
                <Grid item xs={12}>
                    <CustomButton label="Edit" color="primary" icon={<EditIcon />} onClick={options.edit} onClose={options.onClose}/>
                </Grid>
                <Grid item xs={12}>
                    <CustomButton label="Delete" color="primary" icon={<DeleteIcon />} onClick={options.delete} onClose={options.onClose}/>
                </Grid>
            </Grid>
        </React.Fragment>
    )
}

export default SetReminderComponent;