import React from 'react';
import { Grid } from '@material-ui/core';
import CustomBreadCrumb from '../../Shared/CustomBreadCrumb/CustomBreadCrumb';

function BreadCrumbsComponent(props) {
  const { title, label, secondLabel, hasAccount, greetings } = props;
    return(
        <Grid container>
            <CustomBreadCrumb
              title={title} 
              label={label}
              secondLabel={secondLabel}
              hasAccount={hasAccount}
              greetings={greetings}
            />
        </Grid>
    )
}

export default BreadCrumbsComponent;