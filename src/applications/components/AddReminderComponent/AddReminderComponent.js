import React from 'react';
import {
    Grid,
} from '@material-ui/core';

import CustomTextField from '../../Shared/CustomTextField/CustomTextField';

function AddReminderComponent(props) {
    return(
        <React.Fragment>
            <Grid container spacing={2}>
                <Grid item xs={10}>
                    <CustomTextField label="First Name"/>
                </Grid>
                <Grid item xs={2}>
                    <CustomTextField label="M.I"/>
                </Grid>
                <Grid item xs={12}>
                    <CustomTextField label="Last Name"/>
                </Grid>
            </Grid>
            <Grid container spacing={2}>
                <Grid item xs={6}>
                    <CustomTextField label="SSS ID"/>
                </Grid>
                <Grid item xs={6}>
                    <CustomTextField label="PAG-IBIG ID"/>
                </Grid>
            </Grid>
        </React.Fragment>
    )
}

export default AddReminderComponent;