import React, {Fragment} from 'react';
import {
    Typography,
    Grid,
    Toolbar,
} from '@material-ui/core';

function TitleContainerComponent(props) {
    const { label, hasToolbar = true, minHeight, fontWeight } = props;
    return (
        <Fragment>
            <Grid item xs>
                <Typography variant="h6" style={{ fontWeight: fontWeight }}>{label}</Typography>
            </Grid>
            {
                hasToolbar && <Toolbar style={{ minHeight: minHeight }}/>
            }
        </Fragment>
    )
}

export default TitleContainerComponent;