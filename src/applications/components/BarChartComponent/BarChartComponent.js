import React from 'react';
import {
  BarChart, Bar, Cell, XAxis, YAxis, CartesianGrid, Tooltip, Legend,
} from 'recharts';

const data = [
  {
    name: 'January', overtime: 4000, regular: 2400, amt: 2400,
  },
  {
    name: 'February', overtime: 3000, regular: 1398, amt: 2210,
  },
  {
    name: 'March', overtime: 2000, regular: 9800, amt: 2290,
  },
  {
    name: 'April', overtime: 2780, regular: 3908, amt: 2000,
  },
  {
    name: 'May', overtime: 1890, regular: 4800, amt: 2181,
  },
  {
    name: 'June', overtime: 2390, regular: 3800, amt: 2500,
  },
  {
    name: 'July', overtime: 3490, regular: 4300, amt: 2100,
  },
];

function BarChartComponent(props) {
    return (
        <BarChart
            width={500}
            height={300}
            data={data}
            margin={{
            top: 5, right: 30, left: 20, bottom: 5,
            }}
        >
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis dataKey="name" />
            <YAxis />
            <Tooltip />
            <Legend wrapperStyle={{ top: '-1rem' }} />
            <Bar dataKey="regular" fill="#8884d8" />
            <Bar dataKey="overtime" fill="#82ca9d" />
        </BarChart>
    )
}

export default BarChartComponent;