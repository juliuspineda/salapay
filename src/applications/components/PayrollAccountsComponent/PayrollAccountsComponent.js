import React, {useState} from 'react'
import {
    Grid,
    Toolbar,
    Typography,
    Paper,
} from '@material-ui/core';

import HeaderComponent from '../../components/HeaderComponent/HeaderComponent';
import BreadCrumbsComponent from '../../components/BreadCrumbsComponent/BreadCrumbsComponent';
import CustomContainer from '../../Shared/CustomContainer/CustomContainer';

import {makeStyles} from '@material-ui/core/styles';

import { headerData, tableData } from './mock/mock';
import GeneratePDFComponent from '../GeneratePDFComponent/GeneratePDFComponent';
import PaySlipComponent from '../PaySlipComponent/PaySlipComponent';

const useStyles = makeStyles((theme) => ({
    content: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.default,
        padding: theme.spacing(1.5),
    },
    contentPaper: {
        // height: '400px',
        padding: theme.spacing(3),
        alignItems: 'center',
        width: '100%',
    },
    text: {
        margin: '.25rem 0rem .25rem 0rem',
        fontSize: '15px'
    },
    label: {
        margin: '.25rem .5rem .25rem 0rem',
        fontSize: '15px',
        fontWeight: 600
    }
}));

function PayrollAccountsComponent(props) {
    const classes = useStyles();
    
    const [data, setData] = useState(tableData);

    return(
        <div>
            <Grid item xs={12} sm={12} md={12} lg={12} className={classes.content} style={{ padding: '0px' }}>
                <Toolbar/>
                <HeaderComponent />
            </Grid>
            <CustomContainer>
                <Grid xs={12} sm={12} md={12} lg={12}>
                    <Grid container spacing={2}>
                        <Grid item xs={12} sm={12} md={12} lg={12}>
                            <BreadCrumbsComponent title="Payroll" label="Payroll" secondLabel="Accounts" hasAccount/>
                        </Grid>
                        <Grid item xs={12} sm={12} md={12} lg={12}>
                            <Grid container>
                                <Paper className={classes.contentPaper} elevation={0}>
                                    <Grid container fullWidth>
                                        <Grid item xs={12} style={{ marginBottom: '1rem' }}>
                                            <Grid container>
                                                {/* <Grid item xs>
                                                    <Typography variant="h5">Payslip</Typography>
                                                </Grid> */}
                                                <Grid item xs style={{textAlign: 'end'}}>
                                                    <Typography style={{ fontStyle: 'italic' }} color="error" variant="subtitle1">Cut-off Period:01/01/20-01/15/20</Typography>
                                                </Grid>
                                            </Grid>
                                        </Grid>
                                        <Grid item xs={12} style={{ border: '1px solid rgb(237, 237, 237)', padding: '.75rem' }}>
                                            {/* <Grid container>
                                                <Grid item xs={6}>
                                                    <Grid container>
                                                        <Grid item xs={3}>
                                                            <Grid>
                                                                <img src="https://via.placeholder.com/150?text=Photo" alt="placeholder" />
                                                            </Grid>
                                                        </Grid>
                                                        <Grid item xs={9}>
                                                            <Grid
                                                                container
                                                                direction="column"
                                                                justify="center"
                                                                alignItems="flex-start"
                                                                style={{marginLeft: '1rem'}}
                                                            >
                                                                <Grid container>
                                                                    <Typography className={classes.label}>Last Name: </Typography>
                                                                    <Typography className={classes.text}> { data[0].lastName }</Typography>
                                                                </Grid>
                                                                <Grid container>
                                                                    <Typography className={classes.label}>First Name: </Typography>
                                                                    <Typography className={classes.text}> { data[0].firstName }</Typography>
                                                                </Grid>
                                                                <Grid container>
                                                                    <Typography className={classes.label}>Middle Initial: </Typography>
                                                                    <Typography className={classes.text}> { data[0].middleInitial }</Typography>
                                                                </Grid>
                                                                <Grid container>
                                                                    <Typography className={classes.label}>ID Number: </Typography>
                                                                    <Typography className={classes.text}> { data[0].id }</Typography>
                                                                </Grid>
                                                            </Grid>
                                                        </Grid>
                                                    </Grid>
                                                </Grid>
                                                <Grid item xs={6} style={{textAlign: 'end'}}>
                                                    <Grid
                                                        container
                                                        direction="column"
                                                        justify="center"
                                                        alignItems="flex-start"
                                                        style={{marginLeft: '1rem'}}
                                                    >
                                                        <Grid container>
                                                            <Typography className={classes.label}>Total Earnings: </Typography>
                                                            <Typography className={classes.text}> { 'P15,750.00' }</Typography>
                                                        </Grid>
                                                        <Grid container>
                                                            <Typography className={classes.label}>Total Deductions: </Typography>
                                                            <Typography className={classes.text}> { 'P2,752.99' }</Typography>
                                                        </Grid>
                                                        <Grid container>
                                                            <Typography className={classes.label}>Adjustments: </Typography>
                                                            <Typography className={classes.text}> { 'P0.00' }</Typography>
                                                        </Grid>
                                                        <Grid container>
                                                            <Typography className={classes.label}>Net Income: </Typography>
                                                            <Typography className={classes.text}> { 'P13,000.00' }</Typography>
                                                        </Grid>
                                                    </Grid>
                                                </Grid>
                                                <Grid item xs={12}>
                                                <Grid
                                                    container
                                                    style={{marginTop: '1rem'}}
                                                >
                                                        <Grid item xs>
                                                            <Grid container>
                                                                <Typography 
                                                                    variant="h5" 
                                                                    style={{ 
                                                                        margin: '.25rem .5rem .25rem 0rem',
                                                                        fontWeight: 600 
                                                                    }}
                                                                >Earnings</Typography>
                                                            </Grid>
                                                            <Grid container>
                                                                <Typography className={classes.text}>Dispatchment at Marty’s Building x 19 hours (Regular)</Typography>
                                                            </Grid>
                                                            <Grid container>
                                                                <Typography className={classes.text}>Dispatchment at Marty’s Building x 2 hours (OT)</Typography>
                                                            </Grid>
                                                            <Grid container>
                                                                <Typography className={classes.text}>Dispatchment at CorpCor x 75 hours (Regular)</Typography>
                                                            </Grid>
                                                            <Grid container>
                                                                <Typography className={classes.text}>Adjustment (-)</Typography>
                                                            </Grid>
                                                            <Grid container>
                                                                <Typography className={classes.text}>Adjustment (+)</Typography>
                                                            </Grid>
                                                        </Grid>
                                                        <Grid item xs>
                                                            <Grid container>
                                                                <Typography 
                                                                    variant="h5" 
                                                                    style={{ 
                                                                        margin: '.25rem .5rem .25rem 0rem',
                                                                        fontWeight: 600 
                                                                    }}
                                                                >Deductions</Typography>
                                                            </Grid>
                                                            <Grid container>
                                                                <Typography className={classes.text}>SSS (1 out of 2)</Typography>
                                                            </Grid>
                                                            <Grid container>
                                                                <Typography className={classes.text}>Pag-ibig (1 out of 1)</Typography>
                                                            </Grid>
                                                            <Grid container>
                                                                <Typography className={classes.text}>Cash-Advance</Typography>
                                                            </Grid>
                                                            <Grid container>
                                                                <Typography className={classes.text}>Loans</Typography>
                                                            </Grid>
                                                        </Grid>
                                                    </Grid>
                                                </Grid>
                                            </Grid> */}
                                            <PaySlipComponent />
                                            <Grid item xs style={{ padding: '16px', textAlign: 'end' }}>
                                                <GeneratePDFComponent />
                                            </Grid>
                                        </Grid>  
                                    </Grid>
                                </Paper>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </CustomContainer>
        </div>
    )
}

export default PayrollAccountsComponent;