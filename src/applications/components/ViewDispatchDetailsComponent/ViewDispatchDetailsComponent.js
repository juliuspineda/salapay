import React from 'react';
import {
    Grid,
    Typography,
} from '@material-ui/core';

import CustomTextField from '../../Shared/CustomTextField/CustomTextField';
import {makeStyles} from '@material-ui/core/styles';
import TitleContainerComponent from '../TitleContainerComponent/TitleContainerComponent';

const useStyles = makeStyles((theme) => ({
    modalContainer: {
        width: '1000px'
    }
}));

export default function ViewDispatchDetailsComponent() {
    const classes = useStyles();
    return (
        <div>
            <Grid container spacing={2} style={{marginTop:'1rem'}}>
                <Grid item xs={12}>
                    <Grid container spacing={4}>
                        <Grid item xs={6}>
                            <CustomTextField label="Employee Name" size="small" inlineLabel={true} isDisabled={true} value="0"/>
                            <CustomTextField label="Wage Rate" size="small" inlineLabel={true} isDisabled={true} value="0"/>
                            <CustomTextField label="Days Work" size="small" inlineLabel={true} isDisabled={true} value="0"/>
                            <CustomTextField label="Basic Pay" size="small" inlineLabel={true} isDisabled={true} value="0"/>
                            <CustomTextField label="Overtime" size="small" inlineLabel={true} isDisabled={true} value="0"/>
                        </Grid>
                        <Grid item xs={6}>
                            <CustomTextField label="ND Day" size="small" inlineLabel={true} isDisabled={true} value="0"/>
                            <CustomTextField label="ND Pay" size="small" inlineLabel={true} isDisabled={true} value="0"/>
                            <CustomTextField label="Allowance" size="small" inlineLabel={true} isDisabled={true} value="0"/>
                            <CustomTextField label="ADJT" size="small" inlineLabel={true} isDisabled={true} value="0"/>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
            <Grid container spacing={2} style={{marginTop:'1rem'}}>
                <Grid item xs={12}>
                    <Grid container spacing={4}>
                        <Grid item xs={6}>
                            <CustomTextField label="SSS" size="small" inlineLabel={true} isDisabled={true} value="0"/>
                            <CustomTextField label="PhilHealth" size="small" inlineLabel={true} isDisabled={true} value="0"/>
                        </Grid>
                        <Grid item xs={6}>
                            <CustomTextField label="Pag-Ibig" size="small" inlineLabel={true} isDisabled={true} value="0"/>
                            <CustomTextField label="HDMF-MPL" size="small" inlineLabel={true} isDisabled={true} value="0"/>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
            <Grid container spacing={2} style={{marginTop:'1rem'}}>
                <Grid item xs={12}>
                    <Grid container spacing={4}>
                        <Grid item xs={6}>
                            <CustomTextField label="Cash Bond" size="small" inlineLabel={true} isDisabled={true} value="0"/>
                            <CustomTextField label="AR and OE" size="small" inlineLabel={true} isDisabled={true} value="0"/>
                        </Grid>
                        <Grid item xs={6}>
                            <CustomTextField label="Death Contribution" size="small" inlineLabel={true} isDisabled={true} value="0"/>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
            <Grid container spacing={2} style={{marginTop:'1rem'}}>
                <Grid item xs={12}>
                    <Grid container spacing={4}>
                        <Grid item xs={6}>
                            <CustomTextField label="Total Deductions" size="small" inlineLabel={true} isDisabled={true} value="0"/>
                        </Grid>
                        <Grid item xs={6}>
                            <CustomTextField label="Gross Pay" size="small" inlineLabel={true} isDisabled={true} value="0"/>
                            <CustomTextField label="Net Pay" size="small" inlineLabel={true} isDisabled={true} value="0"/>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        </div>
    )
}