import React, { useState, useEffect } from 'react'
import {
    Typography,
    Grid,
    Chip,
} from '@material-ui/core';

import moment from 'moment';

const TITLE_NAME = 'Jasper';

function HeaderComponent() {
    const [timeDashboard, setTimeDashboard] = useState('');
    const [dateDashboard, setDateDashboard] = useState('');

    useEffect(() => {
        setDateDashboard(moment().format('dddd, MMMM D, YYYY'));
        setTimeDashboard(moment().format('h:mm A'))
    },[timeDashboard, setTimeDashboard, dateDashboard, setDateDashboard]);

    return(
        <div></div>
        // <div style={{ backgroundColor: 'white', padding: '1.5rem', borderBottom: '1px solid #ededed' }}>
        //     <Grid container>
        //         <Grid item xs>
        //             <Typography variant="h4">Welcome, {TITLE_NAME}!</Typography>
        //         </Grid>
        //         <Grid item xs style={{ alignSelf: 'center', textAlign: 'end' }}>
        //             <Grid container justify="flex-end">
        //                 <Typography variant="p" style={{alignSelf: 'center',marginRight: '.5rem'}}>Updated as of</Typography>
        //                 <Chip label={timeDashboard} 
        //                     style={{ fontSize: '1rem', background: 'transparent', border: '1px solid #e0e0e0', height: '30px', marginRight: '.5rem' }}
        //                 />
        //                 <Typography variant="h6" style={{alignSelf: 'center'}}>{dateDashboard}</Typography>
        //             </Grid>
        //         </Grid>
        //     </Grid>
        // </div>
    )
}

export default HeaderComponent;