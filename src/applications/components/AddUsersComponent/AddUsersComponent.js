import React from 'react';
import {
    Grid,
} from '@material-ui/core';

import CustomTextField from '../../Shared/CustomTextField/CustomTextField';

export default function AddUsersComponent() {
    return (
        <React.Fragment>
            <Grid container spacing={2}>
                <Grid item xs={12}>
                    <CustomTextField label="Username"/>
                </Grid>
                <Grid item xs={12}>
                    <CustomTextField label="Password"/>
                </Grid>
                <Grid item xs={12}>
                    <CustomTextField label="Phone"/>
                </Grid>
                <Grid item xs={12}>
                    <CustomTextField label="Email"/>
                </Grid>
            </Grid>
        </React.Fragment>
    )
}