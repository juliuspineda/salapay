import React, { Suspense, useState } from 'react';
import { Route, Switch } from "react-router-dom";

import Navigation from './pages/_Navigation/Navigation';
import LoginPage from './pages/LoginPage/LoginPage';
import ErrorOutlineIcon from '@material-ui/icons/ErrorOutline';

import { createMuiTheme,ThemeProvider } from '@material-ui/core/styles';

const theme = createMuiTheme({
  typography: {
    fontSize: 12
  }
});

function App() {

  const [isLogin, setIsLogin] = useState(false);

  return (
    <Suspense fallback={(<div>Loading...</div>)}>
        <div style={{ minHeight: '100vh' }}>
            <ThemeProvider theme={theme}>
              <Switch>
                  <Route path="/" render={ props => {
                      if(isLogin === false) {
                        return <LoginPage isLogin={isLogin} setIsLogin={setIsLogin} {...props} />
                      } 
                      return <Navigation isLogin={isLogin} setIsLogin={setIsLogin} {...props} />   

                      // return <div style={{ display: 'flex', justifyContent: 'center', minHeight: '100vh', alignItems: 'center' }}>
                      //   <ErrorOutlineIcon style={{ width: '50px', height: '50px', color: 'gray' }}/>
                      //   <div>asd</div>
                      // </div>
                  }}/>
              </Switch>
            </ThemeProvider>
        </div>
    </Suspense>
  );
}

export default App;
