import React from 'react';
import { withStyles, useTheme, makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogActions from '@material-ui/core/DialogActions';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';

import CheckCircleOutlineIcon from '@material-ui/icons/CheckCircleOutline';

const styles = (theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(4,4,0,4),
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    //color: theme.palette.grey[500],
    color: '#000'
  },
  title: {
    fontWeight: 600
  }
});

const useStyles = makeStyles({
    rounded: {
        borderRadius: '10px'
    },
    btn: {
        padding: '14px 28px',
        textTransform: 'none',
        fontWeight: 'bold',
        fontSize: '16px',
        //background: '#28a745' 
    }
});

const SuccessBtn = withStyles((theme) => ({
    root: {
        background: '#28a745',
        color: '#fff',
        "&:hover": {
            backgroundColor: '#338e48'
        },
        "&:disabled": {
            backgroundColor: theme.palette.error.light
        }
    },
}))(Button);

const StyledIcon = withStyles((theme) => ({
    root: {
        color: '#28a745',
        fontSize: '100px',
        marginBottom: '1rem'
    },
}))(CheckCircleOutlineIcon);

const DialogTitle = withStyles(styles)((props) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant="h5" className={classes.title}>{children}</Typography>
      {onClose ? (
        <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles((theme) => ({
  root: {
    padding: '16px 100px 0px 100px',
  },
}))(MuiDialogContent);

const DialogActions = withStyles((theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(2,4,4,4),
    justifyContent: 'center'
  },
}))(MuiDialogActions);

function SuccessDialog(props) {
  const {
      onClose,
      onOpen,
      open,
      close,
      label,
      children,
  } = props;

const classes = useStyles();
  
//   const theme = useTheme();
//   const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));

  return (
    <div>
      {/* <Button variant="outlined" color="primary" onClick={onOpen}>
        Open dialog
      </Button> */}
      <Dialog 
        //fullScreen={fullScreen} 
        onClose={onClose} 
        aria-labelledby="customized-dialog-title" 
        open={open}
        PaperProps={{
            classes: { rounded: classes.rounded }
        }}
      >
        <DialogTitle id="customized-dialog-title" onClose={onClose}>
          {/*Submit Successfully*/}
        </DialogTitle>
        <DialogContent
            //dividers
        >
          <Grid container direction="column" alignItems="center">
            <StyledIcon/>
            <Typography variant="h3">Thank You!</Typography>
            <Typography variant="body1">The form was submitted successfully.</Typography>
          </Grid>
        </DialogContent>
        <DialogActions>
          <SuccessBtn color="primary" className={classes.btn} autoFocus onClick={onClose} disableElevation>
            Submit
          </SuccessBtn>
        </DialogActions>
      </Dialog>
    </div>
  );
}

export default SuccessDialog;