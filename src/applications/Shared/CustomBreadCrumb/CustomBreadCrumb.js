import React from 'react';
import { emphasize, withStyles, makeStyles } from '@material-ui/core/styles';
import { 
    Grid, 
    Breadcrumbs,
    Typography,
    Link 
} from '@material-ui/core';
// breadcrumb icons
import Chip from '@material-ui/core/Chip';
import HomeIcon from '@material-ui/icons/Home';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

const BG_COLOR = '#fafafa';

// const StyledBreadcrumb = withStyles((theme) => ({
//     root: {
//       //backgroundColor: theme.palette.grey[100],
//       background: BG_COLOR,
//       height: theme.spacing(3),
//       fontSize: '14px',
//       fontWeight: 'bold',
//       //color: theme.palette.grey[800],
//     //   fontWeight: theme.typography.fontWeightRegular,
//     //   '&:hover, &:focus': {
//     //     backgroundColor: theme.palette.grey[300],
//     //   },
//     //   '&:active': {
//     //     boxShadow: theme.shadows[1],
//     //     backgroundColor: emphasize(theme.palette.grey[300], 0.12),
//     //   },
//     },
//   }))(Chip);

const useStyles = makeStyles((theme) => ({
    link: {
        display: 'flex',
        fontSize: '12px',
        fontWeight: 'bold'
    },
    icon: {
        marginRight: theme.spacing(0.5),
        width: 20,
        height: 20,
    },
}));

function CustomBreadCrumb(props) {
    const {
        title,
        label,
        secondLabel,
        hasAccount,
        greetings
    } = props;
    
    const classes = useStyles();

    return (
        <Grid container>
            {
                title && <Grid xs={12}>
                    <Grid container>
                        <Grid xs>
                            <Typography variant="h5" style={{fontWeight: 'bold'}} gutterBottom>{title}</Typography>
                        </Grid>
                        {/* <Grid xs>
                            <Typography variant="h5" style={{fontWeight: 'bold', textAlign: 'right'}} gutterBottom>{title}</Typography>
                        </Grid> */}
                    </Grid>
                </Grid>
            }
            <Breadcrumbs aria-label="breadcrumb">
                <Link color="inherit" href="/" className={classes.link}>
                    Home
                </Link>
                <Link color="inherit" href="/" className={classes.link}>
                    {label}
                </Link>
                { 
                    hasAccount && <Link color="inherit" href="/" className={classes.link} aria-current="page">
                        {secondLabel}
                    </Link>
                }
                {/* <StyledBreadcrumb
                    component="a"
                    href="#"
                    label="Home"
                    //icon={<HomeIcon fontSize="small" />}
                    // onClick={handleClick}
                /> */}
                {/* <StyledBreadcrumb component="a" href="#" label="Catalog" 
                    //onClick={handleClick} 
                /> */}
                {/* <StyledBreadcrumb
                    label={label}
                    //deleteIcon={<ExpandMoreIcon />}
                    // onClick={handleClick}
                    // onDelete={handleClick}
                /> */}

                {/* {
                    hasAccount && <StyledBreadcrumb
                        label={secondLabel}
                        //deleteIcon={<ExpandMoreIcon />}
                        // onClick={handleClick}
                        // onDelete={handleClick}
                    />
                } */}
            </Breadcrumbs>
            {
                greetings && <Grid xs={12} style={{margin: '.5rem 0rem'}}>
                    <Typography variant="h6">Welcome, Jasper!</Typography>
                </Grid>
            }
        </Grid>
    )
}

export default CustomBreadCrumb;