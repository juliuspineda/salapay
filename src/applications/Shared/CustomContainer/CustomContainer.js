import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import bg from '../../assets/images/bg-content-2.jpg';

const useStyles = makeStyles((theme) => ({
    container: {
        flexGrow: 1,
        //backgroundColor: theme.palette.background.default,
        padding: theme.spacing(3),
        height: '100vh'
    }
}));

function CustomContainer(props) {
    const { children } = props;
    const classes = useStyles();
    return (
        <div 
            className={classes.container} 
            style={{
                background: '#f7f9fc',
                // backgroundImage: `url(${bg})`, 
                // backgroundSize: 'cover', 
                // backgroundPosition: 'center', 
                // backgroundRepeat: 'no-repeat', 
            }}
        >
            {children}
        </div>
    )
}

export default CustomContainer;