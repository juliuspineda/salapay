import React, { useState } from 'react';
import { Button } from '@material-ui/core';
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
pdfMake.vfs = pdfFonts.pdfMake.vfs;

function GeneratePDF(props) {

    const onClickGeneratePDF = () => {

        /**
            pdfMake.createPDF(docs).open();
            pdfMake.createPDF(docs).download();
            pdfMake.createPDF(docs).print(); 
        **/

        // const tableLayouts = {
        //     // can customize table layout without changing the global variable
        // };
        const EMPLOYEE_ID = '20027358';
        const CURRENT_DATE = '2021-02-08';
        const EMPLOYEE_NAME = 'CHRISTIAN GASPAR';

        let docDefinition = {
            pageSize: 'A4',
            pageOrientation: 'landscape', //portrait
            pageMargins: [ 20, 20, 20, 20 ],
            content: [
                { text: 'PAYROLL COVERED', style: 'header', margin: [0, 0, 0, 0] },
                { text: 'Salapay', style: 'logo', alignment: 'center' },
                { text: 'EMPLOYEE DETAILS', style: 'subheader', margin: [0, 15, 0, 0] },
                {
                    style: 'details',
                    margin: [0, 10],
                    columns: [
                        {
                            width: '50%',
                            text: [
                                { text: 'EMPLOYEE ID: ', bold: true }, 
                                `${EMPLOYEE_ID}\n`,
                                { text: 'EMPLOYEE NAME: ', bold: true  }, 
                                `${EMPLOYEE_NAME}\n`,
                            ]
                        },
                        {
                            width: '50%',
                            text: [
                                { text: 'DATE: ', bold: true  }, 
                                `${CURRENT_DATE}`
                            ],
                            alignment: 'right'
                        }
                    ],
                },
                {
                    columns: [
                        {
                            width: '33.33%',
                            style: 'table',
                            margin: [0, 5],
                            table: {
                                headerRows: 1,
                                widths: ['auto', '*', 'auto'],
                                body: [
                                    [
                                        { 
                                            text: 'EARNINGS', 
                                            colSpan: 3, 
                                            style: 'tableHeader'
                                        }, 
                                        {
                                            border: [false, false, false, false],
                                            text: ''
                                        }, 
                                        {
                                            border: [false, false, false, false],
                                            text: ''
                                        }, 
                                    ],
                                    [
                                        {
                                            // left - top - right - bottom
                                            border: [true, true, false, false],
                                            text: 'Total Working Days'
                                        },
                                        {
                                            border: [false, false, false, false],
                                            text: ''
                                        },
                                        {
                                            border: [false, true, true, false],
                                            text: 'P0.00'
                                        },
                                    ],
                                    [
                                        {
                                            // left - top - right - bottom
                                            border: [true, false, false, false],
                                            text: 'Total Overtime Pay'
                                        },
                                        {
                                            border: [false, false, false, false],
                                            text: ''
                                        },
                                        {
                                            border: [false, false, true, false],
                                            text: 'P0.00'
                                        },
                                    ],
                                    [
                                        {
                                            // left - top - right - bottom
                                            border: [true, false, false, false],
                                            text: 'Total Holiday Pay'
                                        },
                                        {
                                            border: [false, false, false, false],
                                            text: ''
                                        },
                                        {
                                            border: [false, false, true, false],
                                            text: 'P0.00'
                                        },
                                    ],
                                    [
                                        {
                                            // left - top - right - bottom
                                            border: [true, false, false, false],
                                            text: 'Total Night Differential'
                                        },
                                        {
                                            border: [false, false, false, false],
                                            text: ''
                                        },
                                        {
                                            border: [false, false, true, false],
                                            text: 'P0.00'
                                        },
                                    ],
                                    [
                                        {
                                            // left - top - right - bottom
                                            border: [true, false, false, false],
                                            text: 'Adjustment from Previous Cut-off'
                                        },
                                        {
                                            border: [false, false, false, false],
                                            text: ''
                                        },
                                        {
                                            border: [false, false, true, false],
                                            text: 'P0.00'
                                        },
                                    ],
                                    [
                                        {
                                            // left - top - right - bottom
                                            border: [true, false, false, false],
                                            text: 'Allowance\n'
                                        },
                                        {
                                            border: [false, false, false, false],
                                            text: ''
                                        },
                                        {
                                            border: [false, false, true, false],
                                            text: 'P0.00'
                                        },
                                    ],
                                    [
                                        {
                                            // left - top - right - bottom
                                            border: [true, false, false, false],
                                            margin: [0, 10, 0, 0],
                                            text: 'Per Detachment Summary'
                                        },
                                        {
                                            border: [false, false, false, false],
                                            margin: [0, 10, 0, 0],
                                            text: 'No. of days'
                                        },
                                        {
                                            border: [false, false, true, false],
                                            text: ''
                                        },
                                    ],
                                    [
                                        {
                                            // left - top - right - bottom
                                            colSpan: 3,
                                            border: [true, false, true, false],
                                            text: '<Detachment 1>'
                                        },
                                        {
                                            border: [false, false, false, true],
                                            text: ''
                                        },
                                        {
                                            border: [false, false, true, false],
                                            text: ''
                                        },
                                    ],
                                    [
                                        {
                                            // left - top - right - bottom
                                            border: [true, false, false, false],
                                            text: 'No. of Working Days <rate>'
                                        },
                                        {
                                            border: [false, false, false, false],
                                            text: '14'
                                        },
                                        {
                                            border: [false, false, true, false],
                                            text: 'P0.00'
                                        },
                                    ],
                                    [
                                        {
                                            // left - top - right - bottom
                                            border: [true, false, false, false],
                                            text: 'Overtime <rate>'
                                        },
                                        {
                                            border: [false, false, false, false],
                                            text: '2'
                                        },
                                        {
                                            border: [false, false, true, false],
                                            text: 'P0.00'
                                        },
                                    ],
                                    [
                                        {
                                            // left - top - right - bottom
                                            border: [true, false, false, false],
                                            text: 'Holidays <rate>'
                                        },
                                        {
                                            border: [false, false, false, false],
                                            text: '2'
                                        },
                                        {
                                            border: [false, false, true, false],
                                            text: 'P0.00'
                                        },
                                    ],
                                    [
                                        {
                                            // left - top - right - bottom
                                            border: [true, false, false, false],
                                            text: 'Night Differential <rate>'
                                        },
                                        {
                                            border: [false, false, false, false],
                                            text: '2'
                                        },
                                        {
                                            border: [false, false, true, false],
                                            text: 'P0.00'
                                        },
                                    ],
                                    [
                                        {
                                            // left - top - right - bottom
                                            colSpan: 3,
                                            border: [true, false, true, false],
                                            margin: [0, 10, 0, 0],
                                            text: '<Detachment 2>'
                                        },
                                        {
                                            border: [false, false, false, true],
                                            text: ''
                                        },
                                        {
                                            border: [false, false, true, false],
                                            text: ''
                                        },
                                    ],
                                    [
                                        {
                                            // left - top - right - bottom
                                            border: [true, false, false, false],
                                            text: 'No. of Working Days <rate>'
                                        },
                                        {
                                            border: [false, false, false, false],
                                            text: '12'
                                        },
                                        {
                                            border: [false, false, true, false],
                                            text: 'P0.00'
                                        },
                                    ],
                                    [
                                        {
                                            // left - top - right - bottom
                                            border: [true, false, false, false],
                                            text: 'Overtime <rate>'
                                        },
                                        {
                                            border: [false, false, false, false],
                                            text: '2'
                                        },
                                        {
                                            border: [false, false, true, false],
                                            text: 'P0.00'
                                        },
                                    ],
                                    [
                                        {
                                            // left - top - right - bottom
                                            border: [true, false, false, false],
                                            text: 'Holidays <rate>'
                                        },
                                        {
                                            border: [false, false, false, false],
                                            text: '2'
                                        },
                                        {
                                            border: [false, false, true, false],
                                            text: 'P0.00'
                                        },
                                    ],
                                    [
                                        {
                                            // left - top - right - bottom
                                            border: [true, false, false, true],
                                            text: 'Night Differential <rate>'
                                        },
                                        {
                                            border: [false, false, false, true],
                                            text: '1'
                                        },
                                        {
                                            border: [false, false, true, true],
                                            text: 'P0.00'
                                        },
                                    ],
                                    // [
                                    //     {
                                    //         // left - top - right - bottom
                                    //         border: [true, true, false, true],
                                    //         text: 'Total Working Days'
                                    //     },
                                    //     {
                                    //         border: [false, true, false, true],
                                    //         text: ''
                                    //     },
                                    //     {
                                    //         border: [false, true, true, true],
                                    //         text: 'P0.00'
                                    //     },
                                    // ],
                                ]
                            },
                            //layout: 'lightHorizontalLines'
                        },
                        {
                            width: '33.33%',
                            style: 'table',
                            margin: [0, 5],
                            table: {
                                headerRows: 1,
                                widths: ['*', '*', 'auto'],
                                body: [
                                    [
                                        { 
                                            text: 'DEDUCTIONS', 
                                            colSpan: 3, 
                                            style: 'tableHeader'
                                        }, 
                                        {
                                            border: [false, false, false, false],
                                            text: ''
                                        }, 
                                        {
                                            border: [false, false, false, false],
                                            text: ''
                                        }, 
                                    ],
                                    [
                                        {
                                            // left - top - right - bottom
                                            border: [true, true, false, false],
                                            text: 'SSS Contribution'
                                        },
                                        {
                                            border: [false, false, false, false],
                                            text: ''
                                        },
                                        {
                                            border: [false, true, true, false],
                                            text: 'P0.00'
                                        },
                                    ],
                                    [
                                        {
                                            // left - top - right - bottom
                                            border: [true, false, false, false],
                                            text: 'Pag-ibig Contribution'
                                        },
                                        {
                                            border: [false, false, false, false],
                                            text: ''
                                        },
                                        {
                                            border: [false, false, true, false],
                                            text: 'P0.00'
                                        },
                                    ],
                                    [
                                        {
                                            // left - top - right - bottom
                                            border: [true, false, false, false],
                                            text: 'Philhealth Contribution'
                                        },
                                        {
                                            border: [false, false, false, false],
                                            text: ''
                                        },
                                        {
                                            border: [false, false, true, false],
                                            text: 'P0.00'
                                        },
                                    ],
                                    [
                                        {
                                            // left - top - right - bottom
                                            border: [true, false, false, false],
                                            text: 'Cash Bond'
                                        },
                                        {
                                            border: [false, false, false, false],
                                            text: ''
                                        },
                                        {
                                            border: [false, false, true, false],
                                            text: 'P0.00'
                                        },
                                    ],
                                    [
                                        {
                                            // left - top - right - bottom
                                            border: [true, false, false, false],
                                            text: 'SSS Loan'
                                        },
                                        {
                                            border: [false, false, false, false],
                                            text: ''
                                        },
                                        {
                                            border: [false, false, true, false],
                                            text: 'P0.00'
                                        },
                                    ],
                                    [
                                        {
                                            // left - top - right - bottom
                                            margin: [0, 10, 0, 0],
                                            border: [true, false, false, false],
                                            text: 'Pag-ibig Loan'
                                        },
                                        {
                                            margin: [0, 10, 0, 0],
                                            border: [false, false, false, false],
                                            text: ''
                                        },
                                        {
                                            margin: [0, 10, 0, 0],
                                            border: [false, false, true, false],
                                            text: 'P0.00'
                                        },
                                    ],
                                    [
                                        {
                                            // left - top - right - bottom
                                            border: [true, false, false, false],
                                            text: 'Paraphernalias'
                                        },
                                        {
                                            border: [false, false, false, false],
                                            text: ''
                                        },
                                        {
                                            border: [false, false, true, false],
                                            text: 'P0.00'
                                        },
                                    ],
                                    [
                                        {
                                            // left - top - right - bottom
                                            border: [true, false, false, false],
                                            text: 'Death Contribution'
                                        },
                                        {
                                            border: [false, false, false, false],
                                            text: ''
                                        },
                                        {
                                            border: [false, false, true, false],
                                            text: 'P0.00'
                                        },
                                    ],
                                    [
                                        {
                                            // left - top - right - bottom
                                            border: [true, false, false, false],
                                            text: 'Security License'
                                        },
                                        {
                                            border: [false, false, false, false],
                                            text: ''
                                        },
                                        {
                                            border: [false, false, true, false],
                                            text: 'P0.00'
                                        },
                                    ],
                                    [
                                        {
                                            // left - top - right - bottom
                                            border: [true, false, false, false],
                                            text: 'Training Expense'
                                        },
                                        {
                                            border: [false, false, false, false],
                                            text: ''
                                        },
                                        {
                                            border: [false, false, true, false],
                                            text: 'P0.00'
                                        },
                                    ],
                                    [
                                        {
                                            // left - top - right - bottom
                                            border: [true, false, false, false],
                                            text: 'Barracks Fee'
                                        },
                                        {
                                            border: [false, false, false, false],
                                            text: ''
                                        },
                                        {
                                            border: [false, false, true, false],
                                            text: 'P0.00'
                                        },
                                    ],
                                    [
                                        {
                                            // left - top - right - bottom
                                            border: [true, false, false, false],
                                            text: 'Insurance Fee'
                                        },
                                        {
                                            border: [false, false, false, false],
                                            text: ''
                                        },
                                        {
                                            border: [false, false, true, false],
                                            text: 'P0.00'
                                        },
                                    ],
                                    [
                                        {
                                            // left - top - right - bottom
                                            border: [true, false, false, false],
                                            text: 'FAS Loss'
                                        },
                                        {
                                            border: [false, false, false, false],
                                            text: ''
                                        },
                                        {
                                            border: [false, false, true, false],
                                            text: 'P0.00'
                                        },
                                    ],
                                    [
                                        {
                                            // left - top - right - bottom
                                            border: [true, false, false, false],
                                            text: 'Fines and Penalties'
                                        },
                                        {
                                            border: [false, false, false, false],
                                            text: ''
                                        },
                                        {
                                            border: [false, false, true, false],
                                            text: 'P0.00'
                                        },
                                    ],
                                    [
                                        {
                                            // left - top - right - bottom
                                            border: [true, false, false, false],
                                            text: 'NTC License'
                                        },
                                        {
                                            border: [false, false, false, false],
                                            text: ''
                                        },
                                        {
                                            border: [false, false, true, false],
                                            text: 'P0.00'
                                        },
                                    ],
                                    [
                                        {
                                            // left - top - right - bottom
                                            border: [true, false, false, false],
                                            text: 'Cash Advance'
                                        },
                                        {
                                            border: [false, false, false, false],
                                            text: ''
                                        },
                                        {
                                            border: [false, false, true, false],
                                            text: 'P0.00'
                                        },
                                    ],
                                    [
                                        {
                                            // left - top - right - bottom
                                            border: [true, false, false, true],
                                            margin: [0, 0, 0, 10],
                                            text: 'Others'
                                        },
                                        {
                                            border: [false, false, false, true],
                                            margin: [0, 0, 0, 10],
                                            text: ''
                                        },
                                        {
                                            border: [false, false, true, true],
                                            margin: [0, 0, 0, 10],
                                            text: 'P0.00'
                                        },
                                    ],
                                ]
                            },
                            //layout: 'lightHorizontalLines'
                        },
                        {
                            width: '33.33%',
                            style: 'table',
                            margin: [0, 5],
                            table: {
                                headerRows: 1,
                                widths: ['*', 'auto'],
                                body: [
                                    [
                                        { 
                                            text: 'LOAN BALANCES', 
                                            colSpan: 2, 
                                            style: 'tableHeader'
                                        }, 
                                        {
                                            border: [false, false, false, false],
                                            text: ''
                                        }, 
                                    ],
                                    [
                                        {
                                            // left - top - right - bottom
                                            border: [true, true, false, false],
                                            text: 'Cash Advance Balance'
                                        },
                                        {
                                            border: [false, true, true, false],
                                            text: 'P0.00'
                                        },
                                    ],
                                    [
                                        {
                                            // left - top - right - bottom
                                            border: [true, false, false, false],
                                            text: 'SSS Loan'
                                        },
                                        {
                                            border: [false, false, true, false],
                                            text: 'P0.00'
                                        },
                                    ],
                                    [
                                        {
                                            // left - top - right - bottom
                                            border: [true, false, false, false],
                                            text: 'Pag-ibig Loan'
                                        },
                                        {
                                            border: [false, false, true, false],
                                            text: 'P0.00'
                                        },
                                    ],
                                    [
                                        {
                                            //margin: [0, 110, 0, 0],
                                            border: [true, false, false, false],
                                            text: 'Taxable Income YTD'
                                        },
                                        {
                                            //margin: [0, 110, 0, 0],
                                            border: [false, false, true, false],
                                            text: 'P0.00'
                                        },
                                    ],
                                    [
                                        {
                                            border: [true, false, false, false],
                                            text: 'Tax Deductions YTD'
                                        },
                                        {
                                            border: [false, false, true, false],
                                            text: 'P0.00'
                                        },
                                    ],
                                    [
                                        {
                                            margin: [0, 78, 0, 0],
                                            border: [true, false, false, false],
                                            text: 'SSS Contribution YTD'
                                        },
                                        {
                                            margin: [0, 20, 0, 0],
                                            border: [false, false, true, false],
                                            text: 'P0.00'
                                        },
                                    ],
                                    [
                                        {
                                            border: [true, false, false, false],
                                            text: 'Pag-ibig Contribution YTD'
                                        },
                                        {
                                            border: [false, false, true, false],
                                            text: 'P0.00'
                                        },
                                    ],
                                    [
                                        {
                                            border: [true, false, false, true],
                                            text: 'Philhealth YTD'
                                        },
                                        {
                                            border: [false, false, true, true],
                                            text: 'P0.00'
                                        },
                                    ],
                                ]
                            },
                            layout: 'lightHorizontalLines'
                        },
                    ],
                    columnGap: 15
                },
                {
                    width: '100%',
                    style: 'table',
                    margin: [0, 20],
                    columns: [
                        {
                            table: {
                                headerRows: 1,
                                widths: ['*', 'auto'],
                                body: [
                                    [
                                        { 
                                            text: 'GROSS PAY',
                                            //bold: true,
                                            style: 'grossPay',
                                            border: [false, false, false, false] 
                                        }, 
                                        {
                                            text: 'P0.00',
                                            bold: true,
                                            style: 'grossPay',
                                            border: [false, false, false, false] 
                                        }, 
                                    ],
                                ]
                            },
                        },
                        {
                            table: {
                                headerRows: 1,
                                widths: ['*', 'auto'],
                                body: [
                                    [
                                        {
                                            text: 'TOTAL DEDUCTIONS',
                                            // bold: true,
                                            style: 'totalDeductions',
                                            border: [false, false, false, false] 
                                        },
                                        {
                                            text: 'P0.00',
                                            bold: true,
                                            style: 'totalDeductions',
                                            border: [false, false, false, false] 
                                        },
                                    ],
                                ]
                            },
                        },
                        {
                            table: {
                                headerRows: 1,
                                widths: ['*', 'auto'],
                                body: [
                                    [
                                        {
                                            text: 'NET PAY',
                                            style: 'netPay',
                                            // bold: true,
                                            border: [false, false, false, false] 
                                        },
                                        {
                                            text: 'P0.00',
                                            style: 'netPay',
                                            bold: true,
                                            border: [false, false, false, false] 
                                        },
                                    ],
                                ]
                            },
                        },
                    ],
                    columnGap: 20
                    // layout: 'lightHorizontalLines'
                },
                {
                    text: 'Notes/Remarks:', style: 'notesAndRemarks', margin: [5, 0]
                }
            ],
            styles: {
                logo: {
                    fontSize: 10,
                    //bold: true,
                    alignment: 'center',
                },
                header: {
                    fontSize: 16,
                    bold: true,
                    alignment: 'center',
                    //decoration: 'underline'
                },
                subheader: {
                    fontSize: 12,
                    bold: true
                },
                details: {
                    fontSize: 10,
                },
                table: {
                    fontSize: 10,
                },
                tableHeader: {
                    bold: true,
                    fontSize: 12
                },
                tableExample: {
                    margin: [0, 5, 0, 15]
                },
                grossPay: {
                    fontSize: 12,
                },
                totalDeductions: {
                    fontSize: 12,
                },
                netPay: {
                    fontSize: 14
                },
                notesAndRemarks: {
                    fontSize: 10
                }
            }
        }

        pdfMake.createPdf(docDefinition).open();
    }

    return (
        <div>
            <Button
                onClick={onClickGeneratePDF} 
                variant="contained" 
                disableElevation 
                style={{
                    background: 'green', 
                    padding: '1rem 2rem', 
                    fontWeight: '700', 
                    color: 'white'
                }}
            >
                PRINT PAYSLIP
            </Button>
        </div>
    )
}

export default GeneratePDF;