import React from 'react';

import {
    TextField
} from '@material-ui/core';

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    container: {
      display: 'flex',
      flexWrap: 'wrap',
    },
    textField: {
      width: '100%',
    },
  }));

function CustomDate(props) {
    const { id, label, defaultValue, fullWidth } = props;
    const classes = useStyles();
    return (
        <TextField
            id={id}
            label={label}
            type="date"
            defaultValue={defaultValue}
            className={classes.textField}
            fullWidth={fullWidth}
            InputLabelProps={{
            shrink: true,
            }}
        />
    )
}

export default CustomDate;