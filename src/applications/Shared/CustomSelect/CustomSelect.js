import React from 'react';
import {
    FormControl,
    InputLabel,
    Select,
    MenuItem
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
        formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
        },
        selectEmpty: {
        marginTop: theme.spacing(2),
        },
        select: {
        "& .MuiSelect-outlined": {
            width: '200px',
            padding: '10px'
        }
    }
}));

function CustomSelect() {

    const classes = useStyles();
    const [age, setAge] = React.useState('');

    const handleChange = (event) => {
        setAge(event.target.value);
    };

    return (
        <div>
            <FormControl variant="outlined" className={classes.formControl}>
                {/* <InputLabel id="demo-simple-select-outlined-label">Select Report</InputLabel> */}
                <Select
                    labelId="demo-simple-select-outlined-label"
                    id="demo-simple-select-outlined"
                    value={age}
                    defaultValue={age}
                    displayEmpty
                    onChange={handleChange}
                    className={classes.select}
                    style={{alignSelf: 'center'}}
                    // label="Select Report"
                >
                    <MenuItem value="">
                        Select Report Type
                    </MenuItem>
                    <MenuItem value={10}>Payroll Report</MenuItem>
                    <MenuItem value={20}>SSS Report</MenuItem>
                    <MenuItem value={30}>Pag-ibig Report</MenuItem>
                </Select>
            </FormControl>
        </div>
    )
}

export default CustomSelect;