import React from 'react';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import { withStyles } from '@material-ui/core/styles';

const StyledTab = withStyles((theme) => ({
    root: {
      //backgroundColor: theme.palette.grey[100],
      fontWeight: 'bold'
      //color: theme.palette.grey[800],
    //   fontWeight: theme.typography.fontWeightRegular,
    //   '&:hover, &:focus': {
    //     backgroundColor: theme.palette.grey[300],
    //   },
    //   '&:active': {
    //     boxShadow: theme.shadows[1],
    //     backgroundColor: emphasize(theme.palette.grey[300], 0.12),
    //   },
    },
  }))(Tab);

function CustomTabs(props) {
    const { value, onChange } = props;

    return (
        <Tabs
            value={value}
            onChange={onChange}
            indicatorColor="primary"
            textColor="primary"
            //centered
        >
            <StyledTab label="Profile" />
            <StyledTab label="Salary Information" />
      </Tabs>
    )
}

export default CustomTabs;