import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import FilledInput from '@material-ui/core/FilledInput';
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import TextField from '@material-ui/core/TextField';
import FormLabel from '@material-ui/core/FormLabel';
import { Typography, Grid } from '@material-ui/core';

const useStyles = makeStyles({
    label: {    
        fontSize: '14px',
        fontWeight: 600,
        margin: '5px 0px'
    }
});

function CustomTextField(props) {
    const { label, size = "medium", inlineLabel, isDisabled, value } = props;
    const classes = useStyles();
    //const [name, setName] = React.useState('Composed TextField');

    // const handleChange = (event) => {
    //     setName(event.target.value);
    // };

    return (
        <div>
            <FormControl
                variant="outlined"
                fullWidth={true}    
                size={size}
                //style={{ margin: '1rem 0rem' }}
            >
                <Grid style={{ display: inlineLabel ? 'flex' : 'grid' }}>
                    {
                        inlineLabel === true ? (
                        <>
                            <Grid item xs={6} style={{alignSelf:'center'}}>
                                <Typography className={classes.label} style={{alignSelf: 'center', marginRight: '1rem'}}>{label}</Typography>
                            </Grid>
                            <Grid item xs={6} style={{margin: '5px 0px'}}>
                                <OutlinedInput
                                    disabled={isDisabled} 
                                    //id="component-simple"
                                    //labelWidth={70} 
                                    value={value} 
                                    //onChange={handleChange}
                                />
                            </Grid>
                        </>
                        ) :
                        (
                            <>
                                <Typography className={classes.label} style={{alignSelf: 'center', marginRight: '1rem'}}>{label}</Typography>
                                <OutlinedInput 
                                    //id="component-simple"
                                    //labelWidth={70} 
                                    //value={name} 
                                    //onChange={handleChange}
                                />
                            </>
                        )
                    }
                </Grid>
            </FormControl>
        </div>
    )
}

export default CustomTextField;