import React from 'react';
import DatePicker from "react-datepicker";
 
import "./styles.scss";
import "react-datepicker/dist/react-datepicker.css";

function CustomDatePicker(props) {
    const { 
        startDate, 
        endDate,
        onChange,
        selected
    } = props;

    return (
        <div>
            <DatePicker
                selected={selected}
                onChange={onChange}
                selectsStart
                startDate={startDate}
                endDate={endDate}
                //minDate={startDate}
            />
        </div>
    )
}

export default CustomDatePicker;