import React from 'react';
import {
    Button,
} from '@material-ui/core';

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    button: {
        margin: theme.spacing(1),
        fontSize: '18px',
        textTransform: 'none'
    },
}));

function CustomButton(props) {
    const { label, icon, color, onClick, onClose } = props;
    const classes = useStyles();
    return (
        <Button
            onClick={onClick}
            onClose={onClose}
            variant="contained"
            color={color}
            size="large"
            className={classes.button}
            startIcon={icon}
            fullWidth
            disableElevation
        >
            {label}
        </Button>
    )
}

export default CustomButton;