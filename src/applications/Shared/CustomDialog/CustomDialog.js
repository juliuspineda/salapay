import React from 'react';
import { withStyles, useTheme, makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogActions from '@material-ui/core/DialogActions';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';

//import useMediaQuery from '@material-ui/core/useMediaQuery';

const styles = (theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(4,4,0,4),
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    //color: theme.palette.grey[500],
    color: '#000'
  },
  title: {
    fontWeight: 600
  }
});

const useStyles = makeStyles({
    rounded: {
        borderRadius: '10px'
    },
    btn: {
        padding: '14px 28px',
        textTransform: 'none',
        fontWeight: 'bold',
        fontSize: '16px',
        //background: '#28a745' 
    }
});

const SubmitBtn = withStyles((theme) => ({
    root: {
        background: '#28a745',
        color: '#fff',
        "&:hover": {
            backgroundColor: '#338e48'
        },
        "&:disabled": {
            backgroundColor: theme.palette.error.light
        }
    },
}))(Button);

const DeleteBtn = withStyles((theme) => ({
  root: {
      background: '#eb2f2f',
      color: '#fff',
      "&:hover": {
          backgroundColor: '#ff0000'
      },
      "&:disabled": {
          backgroundColor: theme.palette.error.light
      }
  },
}))(Button);

const ClearBtn = withStyles((theme) => ({
    root: {
        background: '#FFBB28',
        color: '#fff',
        "&:hover": {
            backgroundColor: '#eaae2b'
        },
        "&:disabled": {
            backgroundColor: theme.palette.error.light
        }
    },
}))(Button);

// const StyledDialog = withStyles((theme) => ({
//   paperWidthSm: {
//     width: 'auto'
//   }
// }))(Dialog);

const DialogTitle = withStyles(styles)((props) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant="h5" className={classes.title}>{children}</Typography>
      {onClose ? (
        <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles((theme) => ({
  root: {
    padding: theme.spacing(4),
  },
}))(MuiDialogContent);

const DialogActions = withStyles((theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(2,4,4,4),
  },
}))(MuiDialogActions);

function CustomDialog(props) {
  const {
      onClose,
      onOpen,
      open,
      close,
      label,
      children,
      onSuccess,
      title,
      showButton,
      showSubmitBtn = true,
      showDeleteBtn,
      width
  } = props;

const classes = useStyles();

  return (
    <div>
      {/* <Button variant="outlined" color="primary" onClick={onOpen}>
        Open dialog
      </Button> */}
      <Dialog 
        //fullScreen={fullScreen} 
        onClose={onClose}
        aria-labelledby="customized-dialog-title" 
        open={open}
        PaperProps={{
            classes: { rounded: classes.rounded }
        }}
        maxWidth="false"
      >
        <div style={{width: width }}>
          <DialogTitle id="customized-dialog-title" onClose={onClose}>
            {title}
          </DialogTitle>
          <DialogContent
              //dividers
          >
            {children}
          </DialogContent>
          {
            showButton && (
              <DialogActions>
                <ClearBtn color="primary" className={classes.btn} autoFocus onClick={onClose} disableElevation>
                  Cancel
                </ClearBtn>
                {
                  showSubmitBtn && 
                  <SubmitBtn
                    onClick={onSuccess}
                    color="primary" 
                    className={classes.btn} 
                    autoFocus
                    disableElevation
                  >
                    Submit
                  </SubmitBtn>
                }
                {
                  showDeleteBtn && 
                    <DeleteBtn
                    onClick={onSuccess}
                    color="primary" 
                    className={classes.btn} 
                    autoFocus
                    disableElevation
                  >
                    Delete
                  </DeleteBtn>
                }
              </DialogActions>
            )
          }
        </div>
      </Dialog>
    </div>
  );
}

export default CustomDialog;