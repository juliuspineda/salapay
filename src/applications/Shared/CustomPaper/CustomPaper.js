import React from 'react';
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    container: {
        alignItems: 'center',
        width: '100%',
        border: '1px solid #ededed',
        boxShadow: '0 1px 1px 0 rgb(0 0 0 / 20%)',
    }
}));

function CustomPaper(props) {
    const {children, isTabs, marginBottom, borderClass, noPadding} = props;
    const classes = useStyles();
    return (
        <Paper className={classes.container} style={{ marginBottom: marginBottom ? '30px' : '0', borderRadius: borderClass, padding: noPadding ? 0 : isTabs ? 0 : '32px'}}>
            {children}
        </Paper>
    )
}

export default CustomPaper;