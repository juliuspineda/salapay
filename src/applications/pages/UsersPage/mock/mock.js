import Avatar from '@material-ui/core/Avatar';
export const headerData = [
    // {
    //     title: 'Photo', field: 'photo',
    //     render: (rowData) =>
    //     rowData && (
    //       <Avatar style={{ height: '35px', width: '35px' }}/>
    //     )
    // },
    {
      title: 'Username', field: 'username',
    },
    { title: 'Password', field: 'password' },
    { 
        title: 'Phone', 
        field: 'phone', 
        //type: 'numeric' 
    },
    { 
        title: 'Email', 
        field: 'email', 
        //type: 'numeric' 
    },
  ];

export const tableData = [
    { username: 'jeff123', password: '123321', phone: '09151515151', email: 'jeff@gmail.com' },
    { username: 'michael123', password: '123321', phone: '09151515151', email: 'michael@gmail.com' },
    { username: 'dan123', password: '123321', phone: '09151515151', email: 'dan@gmail.com' },
    { username: 'dannie123', password: '123321', phone: '09151515151', email: 'dannie@gmail.com' },
    { username: 'johnmart123', password: '123321', phone: '09151515151', email: 'johnmart@gmail.com' }
];