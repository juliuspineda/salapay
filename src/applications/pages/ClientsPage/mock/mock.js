import Avatar from '@material-ui/core/Avatar'

export const headerData = [
    {
        title: 'Photo', field: 'photo',
        render: (rowData) =>
        rowData && (
          <Avatar style={{ height: '35px', width: '35px' }}/>
        )
    },
    {
      title: 'Id Number', field: 'id',
      editComponent: props => (
        <input
          type="text"
          value={props.value}
          onChange={e => props.onChange(e.target.value)}
        />
      )
    },
    { title: 'Last Name', field: 'lastName' },
    { 
        title: 'First Name', 
        field: 'firstName', 
        //type: 'numeric' 
    },
    { 
        title: 'M.I.', 
        field: 'middleInitial', 
        //type: 'numeric' 
    },
    {
      title: 'Total Earnings',
      field: 'totalEarnings',
      //lookup: { 34: 'İstanbul', 63: 'Şanlıurfa' },
    },
    {
        title: 'Net Pay',
        field: 'netPay'
      },
  ];

export const tableData = [
    { photo: '', id: 'NS-0001', lastName: 'Dela Cruz', firstName: 'Michael', middleInitial: 'B', totalEarnings: (100000).toFixed(2), netPay: (20000).toFixed(2) },
    { photo: '', id: 'NS-0002', lastName: 'Santos', firstName: 'Dan', middleInitial: 'A', totalEarnings: (100000).toFixed(2), netPay: (20000).toFixed(2) },
    { photo: '', id: 'NS-0003', lastName: 'Torres', firstName: 'Jeffrey', middleInitial: 'D', totalEarnings: (100000).toFixed(2), netPay: (20000).toFixed(2) },
    { photo: '', id: 'NS-0004', lastName: 'Toralba', firstName: 'Danni', middleInitial: 'P', totalEarnings: (100000).toFixed(2), netPay: (20000).toFixed(2) },
    { photo: '', id: 'NS-0005', lastName: 'Alban', firstName: 'John Mart', middleInitial: 'S', totalEarnings: (100000).toFixed(2), netPay: (20000).toFixed(2) },
    { photo: '', id: 'NS-0001', lastName: 'Dela Cruz', firstName: 'Michael', middleInitial: 'B', totalEarnings: (100000).toFixed(2), netPay: (20000).toFixed(2) },
    { photo: '', id: 'NS-0002', lastName: 'Santos', firstName: 'Dan', middleInitial: 'A', totalEarnings: (100000).toFixed(2), netPay: (20000).toFixed(2) },
    { photo: '', id: 'NS-0003', lastName: 'Torres', firstName: 'Jeffrey', middleInitial: 'D', totalEarnings: (100000).toFixed(2), netPay: (20000).toFixed(2) },
    { photo: '', id: 'NS-0004', lastName: 'Toralba', firstName: 'Danni', middleInitial: 'P', totalEarnings: (100000).toFixed(2), netPay: (20000).toFixed(2) },
    { photo: '', id: 'NS-0005', lastName: 'Alban', firstName: 'John Mart', middleInitial: 'S', totalEarnings: (100000).toFixed(2), netPay: (20000).toFixed(2) }
];