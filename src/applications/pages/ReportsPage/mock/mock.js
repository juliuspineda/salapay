import Avatar from '@material-ui/core/Avatar';
export const headerData = [
    {
      title: 'Sr. No.', field: 'srNo'
    },
    {
      title: 'Payable Location', field: 'payableLocation'
    },
    {
      title: 'DR A/C No.', field: 'acNo'
    },
    {
      title: 'Instrument Ref. No.', field: 'instrumentRefNo',
    },
    {
      title: 'Customer Ref. No.', field: 'customerRefNo',
    },
    {
      title: 'Payee A/C No.', field: 'payeeACNo',
    },
    {
      title: 'Payee Name', field: 'payeeName',
    },
    { 
      title: 'Instrument No.', field: 'intrumentNo' 
    },
    { 
      title: 'CR Branch Code', field: 'crBranchCode' 
    },
    { 
      title: 'Value Date', field: 'valueDate' 
    },
    {
      title: 'Debit Date', field: 'debitDate',
    },
    {
      title: 'Instrument Amount', field: 'instrumentAmount',
    },
    {
      title: 'Issuing Branch', field: 'issuingBranch',
    },
    {
      title: 'Aging Days', field: 'agingDays',
    },
    {
      title: 'Liquidation Date', field: 'liquidationDate',
    },
    {
      title: 'Txn. Status', field: 'txnStatus',
    }
  ];

export const tableData = [
  {
    srNo: 1,
    payableLocation: 'Quezon City',
    acNo: 150470003307,
    instrumentRefNo: 'BATCH_1993541_1',
    customerRefNo: 'NHAB1013120',
    payeeACNo: '150410067017',
    payeeName: 'Jerrie Abalos',
    intrumentNo: '1001',
    crBranchCode: 8,
    valueDate: '31/01/2020',
    debitDate: '31/01/2020',
    instrumentAmount: 7854.60,
    issuingBranch: 1504,
    agingDays: 0,
    liquidationDate: 0,
    txnStatus: 'Manual Payment Q'
  },
  {
    srNo: 2,
    payableLocation: 'Malabon City',
    acNo: 150470003307,
    instrumentRefNo: 'BATCH_1993541_10',
    customerRefNo: 'NHAB1013120',
    payeeACNo: '150410067018',
    payeeName: 'Justin Alsado',
    intrumentNo: '1001',
    crBranchCode: 8,
    valueDate: '31/01/2020',
    debitDate: '31/01/2020',
    instrumentAmount: 4631.81,
    issuingBranch: 1504,
    agingDays: 0,
    liquidationDate: 0,
    txnStatus: 'Manual Payment Q'
  },
  {
    srNo: 3,
    payableLocation: 'Valenzuela City',
    acNo: 150470003307,
    instrumentRefNo: 'BATCH_1993541_101',
    customerRefNo: 'NHAB1013120',
    payeeACNo: '150410067017',
    payeeName: 'Ray Metante',
    intrumentNo: '1001',
    crBranchCode: 8,
    valueDate: '31/01/2020',
    debitDate: '31/01/2020',
    instrumentAmount: 2954.70,
    issuingBranch: 1504,
    agingDays: 0,
    liquidationDate: 0,
    txnStatus: 'Manual Payment Q'
  },
  {
    srNo: 4,
    payableLocation: 'Paranaque City',
    acNo: 150470003307,
    instrumentRefNo: 'BATCH_1993541_101',
    customerRefNo: 'NHAB1013120',
    payeeACNo: '150410067017',
    payeeName: 'Don Ares',
    intrumentNo: '1001',
    crBranchCode: 8,
    valueDate: '31/01/2020',
    debitDate: '31/01/2020',
    instrumentAmount: 2954.70,
    issuingBranch: 1504,
    agingDays: 0,
    liquidationDate: 0,
    txnStatus: 'Manual Payment Q'
  },
  {
    srNo: 5,
    payableLocation: 'Cabanatuan City',
    acNo: 150470003307,
    instrumentRefNo: 'BATCH_1993541_101',
    customerRefNo: 'NHAB1013120',
    payeeACNo: '150410067017',
    payeeName: 'Kite Maniebo',
    intrumentNo: '1001',
    crBranchCode: 8,
    valueDate: '31/01/2020',
    debitDate: '31/01/2020',
    instrumentAmount: 2954.70,
    issuingBranch: 1504,
    agingDays: 0,
    liquidationDate: 0,
    txnStatus: 'Manual Payment Q'
  }
];