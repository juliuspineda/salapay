import React, {useState} from 'react'
import {
    Grid,
    Toolbar,
    Typography,
    Button
} from '@material-ui/core';

import HeaderComponent from '../../components/HeaderComponent/HeaderComponent';
import BreadCrumbsComponent from '../../components/BreadCrumbsComponent/BreadCrumbsComponent';
import TableComponent from '../../components/TableComponent/TableComponent';

import {makeStyles} from '@material-ui/core/styles';
import { headerData, tableData } from './mock/mock';

import CustomSelect from '../../Shared/CustomSelect/CustomSelect';
import CustomDatePicker from '../../Shared/CustomDatePicker/CustomDatePicker';
import CustomContainer from '../../Shared/CustomContainer/CustomContainer';

//import "./ReportsPage.css";

const useStyles = makeStyles((theme) => ({
    content: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.default,
        padding: theme.spacing(1.5),
    },
    contentPaper: {
        // height: '400px',
        padding: theme.spacing(3),
        alignItems: 'center',
        width: '100%',
    },
    text: {
        fontWeight:'700', 
        //padding: '.5rem 2rem'
    },
}));

function ReportsPage(props) {
    const classes = useStyles();
    const [startDate, setStartDate] = useState(new Date());
    const [endDate, setEndDate] = useState(new Date());
    const [data, setData] = useState(tableData);
    const [hasShowTable, setHasShowTable] = useState(false);

    return(
        <div>
            <Grid item xs={12} sm={12} md={12} lg={12} className={classes.content} style={{ padding: '0px' }}>
                <Toolbar/>
                <HeaderComponent />
            </Grid>
            <CustomContainer>
                <Grid xs={12} sm={12} md={12} lg={12}>
                    <Grid container spacing={2}>
                        <Grid item xs={12} sm={12} md={12} lg={12}>
                            <BreadCrumbsComponent title="Reports" label="Reports" />
                        </Grid>
                        <Grid item xs={12} sm={12} md={12} lg={12}>
                            <Grid container spacing={2}>
                                <Grid item xs>
                                    <Grid container className={classes.contentPaper}>
                                        <Grid item xs={12}>
                                            <Grid container>    
                                                <Typography style={{marginRight:'.5rem', alignSelf: 'center'}}>FROM</Typography>
                                                <div style={{alignSelf: 'center'}}>
                                                    <CustomDatePicker 
                                                        startDate={startDate}
                                                        endDate={endDate}
                                                        onChange={ (date) => setStartDate(date) }
                                                        selected={startDate}
                                                    />
                                                </div>
                                                <Typography style={{margin: '0rem .5rem', alignSelf: 'center'}}>TO</Typography>
                                                <div style={{alignSelf: 'center'}}>
                                                    <CustomDatePicker 
                                                        startDate={startDate}
                                                        endDate={endDate}
                                                        onChange={ (date) => setEndDate(date) }
                                                        selected={endDate}
                                                    />
                                                </div>
                                                <div style={{margin: '0 .5rem'}}>
                                                    <CustomSelect />
                                                </div>
                                                <div style={{alignSelf: 'center', marginRight: '1rem'}}>
                                                    <Button 
                                                        variant="contained" 
                                                        color="primary"
                                                        style={{ textDecoration: 'underline' }} 
                                                        disableElevation 
                                                        className={classes.text}
                                                        onClick={() => setHasShowTable(!hasShowTable)}
                                                    >
                                                        SELECT
                                                    </Button>
                                                </div>
                                                <div style={{alignSelf: 'center'}}>
                                                    <Button variant="contained" color="primary" disableElevation className={classes.text}>
                                                        CUSTOMIZE REPORT
                                                    </Button>
                                                </div>
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid item xs={12} sm={12} md={12} lg={12}>
                            {
                                hasShowTable && <TableComponent title="Reports" columns={headerData} data={data} setData={setData} {...props}/>
                            }
                        </Grid>
                    </Grid>
                </Grid>
            </CustomContainer>
        </div>
    )
}

export default ReportsPage;