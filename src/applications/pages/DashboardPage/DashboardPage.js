import React,{useState} from 'react'
import {
    Toolbar,
    Typography,
    Grid,
    Paper,
} from '@material-ui/core';

import {makeStyles} from '@material-ui/core/styles';

import PieChartComponent from '../../components/PieChartComponent/PieChartComponent';
import BarChartComponent from '../../components/BarChartComponent/BarChartComponent';

import HeaderComponent from '../../components/HeaderComponent/HeaderComponent';
import BreadCrumbsComponent from '../../components/BreadCrumbsComponent/BreadCrumbsComponent';
import TitleContainerComponent from '../../components/TitleContainerComponent/TitleContainerComponent';
import FloatingButtonComponent from '../../components/FloatingButtonComponent/FloatingButtonComponent';
import SetReminderComponent from '../../components/SetReminderComponent/SetReminderComponent';

import CustomDialog from '../../Shared/CustomDialog/CustomDialog';
import CustomDate from '../../Shared/CustomDate/CustomDate';
import CustomTextField from '../../Shared/CustomTextField/CustomTextField';
import CustomContainer from '../../Shared/CustomContainer/CustomContainer';
import SuccessDialog from '../../Shared/SuccessDialog/SuccessDialog';
import CustomPaper from '../../Shared/CustomPaper/CustomPaper';

// parent styles
const useStyles = makeStyles((theme) => ({
    content: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.default,
        padding: theme.spacing(1.5),
    },
    modalContainer: {
        width: '350px'
    },
    label: {    
        fontSize: '14px',
        fontWeight: 600,
        margin: '5px 0px'
    }
}));

// Mock data
const data = [
    { name: 'Regular', value: 400 }, { name: 'Inactive', value: 300 },
    { name: 'Resigned', value: 300 }, { name: 'Contractual', value: 200 },
];

function AddReminderContainer() {
    const classes = useStyles();
    return (
        <Grid className={classes.modalContainer} container direction="column" spacing={2}>
            <Grid item xs={12}>
                <CustomTextField label="Title"/>
            </Grid>
            <Grid item xs={12}>
                <CustomTextField label="Description"/>
            </Grid>
            <Grid item xs={12}>
                <Typography className={classes.label}>Date</Typography>
                <CustomDate 
                    id="date"
                    defaultValue="2017-05-24"
                    fullWidth
                />
            </Grid>
        </Grid>
    )
}

function EditReminderContainer() {
    const classes = useStyles();
    return (
        <Grid className={classes.modalContainer} container direction="column" spacing={2}>
            <Grid item xs={12}>
                <CustomTextField label="Title"/>
            </Grid>
            <Grid item xs={12}>
                <CustomTextField label="Description"/>
            </Grid>
            <Grid item xs={12}>
                <Typography className={classes.label}>Date</Typography>
                <CustomDate 
                    id="date"
                    defaultValue="2017-05-24"
                    fullWidth
                />
            </Grid>
        </Grid>
    )
}

function DeleteReminderContainer() {
    const classes = useStyles();
    return (
        <Grid className={classes.modalContainer} container spacing={2}>
            <Grid item xs={12}>
                <Typography className={classes.label}>Are you sure you want to delete?</Typography>
            </Grid>
        </Grid>
    )
}

function DashboardPage(props) {
    console.log('props - ',props)
    const classes = useStyles();

    const [reminderModal, setReminderModal] = React.useState(false);
    const [addReminder, setAddReminder] = React.useState(false);
    const [editReminder, setEditReminder] = React.useState(false);
    const [deleteReminder, setDeleteReminder] = React.useState(false);
    const [successModal, setSuccessModal] = useState(false);

    const onClickReminderModal = () => {
        setReminderModal(true);
        setSuccessModal(false);
        setAddReminder(false);
        setEditReminder(false);
        setDeleteReminder(false);
    }

    const handleOnSuccess = () => {
        setSuccessModal(true);
        setReminderModal(false);
        setAddReminder(false);
        setEditReminder(false);
        setDeleteReminder(false);
    }

    const handleClose = () => {
        setReminderModal(false);
        setAddReminder(false);
        setEditReminder(false);
        setDeleteReminder(false);
        setSuccessModal(false);
    }

    const onAddReminder = () => {
        setAddReminder(true);
        setReminderModal(false);
        setEditReminder(false);
        setDeleteReminder(false);
        setSuccessModal(false);
    }

    const onEditReminder = () => {
        setEditReminder(true);
        setReminderModal(false);
        setAddReminder(false);
        setDeleteReminder(false);
        setSuccessModal(false);
    }

    const onDeleteReminder = () => {
        setDeleteReminder(true);
        setReminderModal(false);
        setAddReminder(false);
        setEditReminder(false);
        setSuccessModal(false);
    }
    
    const options = {
        add: () => onAddReminder(),
        edit: () => onEditReminder(),
        delete: () => onDeleteReminder()
    }

    const title = addReminder ? 'Add Reminder' : editReminder ? 'Edit Reminder' : deleteReminder ? 'Delete Reminder' : '';
    const modal = addReminder ? addReminder 
        : editReminder ? editReminder : deleteReminder ? deleteReminder : false;
    const children = addReminder ? <AddReminderContainer /> 
    : editReminder ? <EditReminderContainer /> : deleteReminder ? <DeleteReminderContainer /> : '';

    return (
        <div>
            <Grid item xs={12} sm={12} md={12} lg={12} className={classes.content} style={{ padding: '0px' }}>
                <Toolbar/>
            </Grid>
            <CustomContainer>
                <Grid xs={12} sm={12} md={12} lg={12}>
                    <Grid container spacing={2}>
                        <Grid item xs={12} sm={12} md={12} lg={12}>
                            <BreadCrumbsComponent title="Dashboard" label="Dashboard"/>
                        </Grid>
                        <Grid item xs={6} sm={6} md={6} lg={6}>
                            <Grid container>
                                <CustomPaper>
                                    <Grid container fullWidth>
                                        <TitleContainerComponent label="Overview" />
                                        <PieChartComponent width="100%" height="300px" data={data} hasToolTip />
                                    </Grid>
                                </CustomPaper>
                            </Grid>
                        </Grid>
                        <Grid item xs={6} sm={6} md={6} lg={6}>
                            <Grid container>
                                <CustomPaper>
                                    <Grid container fullWidth>
                                        <TitleContainerComponent label="Working Hours: Regular vs Overtime" />
                                        <BarChartComponent />
                                    </Grid>
                                </CustomPaper>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
                <Grid xs={12} sm={12} md={12} lg={12} style={{marginTop: '.5rem'}}>
                    <Grid container spacing={2}>
                        <Grid item xs={12} sm={12} md={12} lg={12}>
                            <CustomPaper>
                                <Grid container fullWidth>
                                    <TitleContainerComponent label="Reminder" />
                                    <Grid item xs={12}>
                                        <Typography variant="h6">Payroll Release on the 02/15/20</Typography>
                                    </Grid>
                                    <Grid item xs={12}>
                                        <Typography variant="h6">Due of SSS payment endorsement 04/01/20</Typography>
                                    </Grid>
                                </Grid>
                            </CustomPaper>
                        </Grid>
                    </Grid>
                </Grid>
            </CustomContainer>
            <FloatingButtonComponent onClick={onClickReminderModal}/>
            <CustomDialog 
                open={ reminderModal }
                onClose={handleClose}
                title="Set Reminder"
                children={ <SetReminderComponent options={options} {...options}/> }
                onSuccess={ handleOnSuccess }
            />
            <CustomDialog 
                open={ modal }
                onClose={handleClose}
                title={ title }
                children={ children }
                onSuccess={ handleOnSuccess }
                showSubmitBtn={ deleteReminder ? false : true }
                showDeleteBtn={ deleteReminder ? true : false }
                showButton
            />
            <SuccessDialog 
                open={successModal}
                onClose={handleClose}
            />
        </div>
    );
}

export default DashboardPage;
