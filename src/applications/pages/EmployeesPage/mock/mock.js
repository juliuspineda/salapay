import Avatar from '@material-ui/core/Avatar'

export const headerData = [
    {
        title: 'Photo', field: 'photo',
    },
    {
      title: 'Id Number', field: 'id',
      editComponent: props => (
        <input
          type="text"
          value={props.value}
          onChange={e => props.onChange(e.target.value)}
        />
      )
    },
    { title: 'Last Name', field: 'lastName' },
    { 
        title: 'First Name', 
        field: 'firstName', 
        //type: 'numeric' 
    },
    { 
        title: 'M.I.', 
        field: 'middleInitial', 
        //type: 'numeric' 
    },
    {
      title: 'SSS ID',
      field: 'sssID',
      //lookup: { 34: 'İstanbul', 63: 'Şanlıurfa' },
    }
  ];

export const tableData = [
    { photo: '', id: 'NS-0001', lastName: 'Dela Cruz', firstName: 'Michael', middleInitial: 'B', sssID: '04-0751449-0'},
    { photo: '', id: 'NS-0002', lastName: 'Santos', firstName: 'Dan', middleInitial: 'A', sssID: '04-0751448-0'},
    { photo: '', id: 'NS-0003', lastName: 'Torres', firstName: 'Jeffrey', middleInitial: 'D', sssID: '04-0751447-0'},
    { photo: '', id: 'NS-0004', lastName: 'Toralba', firstName: 'Danni', middleInitial: 'P', sssID: '04-0751446-0'},
    { photo: '', id: 'NS-0005', lastName: 'Alban', firstName: 'John Mart', middleInitial: 'S', sssID: '04-0751445-0'},
    { photo: '', id: 'NS-0001', lastName: 'Dela Cruz', firstName: 'Michael', middleInitial: 'B', sssID: '04-0751444-0'},
    { photo: '', id: 'NS-0002', lastName: 'Santos', firstName: 'Dan', middleInitial: 'A', sssID: '04-0751443-0'},
    { photo: '', id: 'NS-0003', lastName: 'Torres', firstName: 'Jeffrey', middleInitial: 'D', sssID: '04-0751442-0'},
    { photo: '', id: 'NS-0004', lastName: 'Toralba', firstName: 'Danni', middleInitial: 'P', sssID: '04-0751441-0' },
    { photo: '', id: 'NS-0005', lastName: 'Alban', firstName: 'John Mart', middleInitial: 'S', sssID: '04-0751440-0'}
];