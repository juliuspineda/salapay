import React, {useState} from 'react'
import {
    Grid,
    Toolbar,
} from '@material-ui/core';

import HeaderComponent from '../../components/HeaderComponent/HeaderComponent';
import BreadCrumbsComponent from '../../components/BreadCrumbsComponent/BreadCrumbsComponent';
import TableComponent from '../../components/TableComponent/TableComponent';
import CustomContainer from '../../Shared/CustomContainer/CustomContainer';

import {makeStyles} from '@material-ui/core/styles';

import { headerData, tableData } from './mock/mock';

const useStyles = makeStyles((theme) => ({
    content: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.default,
        padding: theme.spacing(1.5),
    },
    contentPaper: {
        // height: '400px',
        padding: theme.spacing(3),
        alignItems: 'center',
        width: '100%',
    }
}));

function OrganizationsPage(props) {
    const classes = useStyles();
    
    const [data, setData] = useState(tableData);

    return(
        <div>
            <Grid item xs={12} sm={12} md={12} lg={12} className={classes.content} style={{ padding: '0px' }}>
                <Toolbar/>
                <HeaderComponent />
            </Grid>
            <CustomContainer>
                <Grid xs={12} sm={12} md={12} lg={12}>
                    <Grid container spacing={2}>
                        <Grid item xs={12} sm={12} md={12} lg={12}>
                            <BreadCrumbsComponent title="Clients" label="Clients"/>
                        </Grid>
                        <Grid item xs={12} sm={12} md={12} lg={12}>
                            <Grid container>
                                <Grid item xs>
                                    <TableComponent title="List" columns={headerData} data={data} setData={setData} {...props}/>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </CustomContainer>
        </div>
    )
}

export default OrganizationsPage;