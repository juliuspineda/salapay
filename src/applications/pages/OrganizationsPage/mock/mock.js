import Avatar from '@material-ui/core/Avatar';
export const headerData = [
    {
      title: 'First Name', field: 'firstName',
    },
    { title: 'Last Name', field: 'lastName' },
    { 
        title: 'Username', 
        field: 'userName', 
        //type: 'numeric' 
    },
    { 
        title: 'Email', 
        field: 'email', 
        //type: 'numeric' 
    },
    { 
      title: 'Password', 
      field: 'password', 
      //type: 'numeric' 
    },
    { 
      title: 'Client ID', 
      field: 'clientID', 
      //type: 'numeric' 
    },
    { 
      title: 'Company Name', 
      field: 'companyName', 
      //type: 'numeric' 
    }
  ];

export const tableData = [
    { firstName: 'Jeff', lastName: 'Supang', clientID: '111', companyName: 'Man Services Inc', userName: 'jeff123', password: '123321', phone: '09151515151', email: 'jeff@gmail.com' },
    { firstName: 'Jeff', lastName: 'Supang', clientID: '111', companyName: 'Man Services Inc', userName: 'michael123', password: '123321', phone: '09151515151', email: 'michael@gmail.com' },
    { firstName: 'Jeff', lastName: 'Supang', clientID: '111', companyName: 'Man Services Inc', userName: 'dan123', password: '123321', phone: '09151515151', email: 'dan@gmail.com' },
    { firstName: 'Jeff', lastName: 'Supang', clientID: '111', companyName: 'Man Services Inc', userName: 'dannie123', password: '123321', phone: '09151515151', email: 'dannie@gmail.com' },
    { firstName: 'Jeff', lastName: 'Supang', clientID: '111', companyName: 'Man Services Inc', userName: 'johnmart123', password: '123321', phone: '09151515151', email: 'johnmart@gmail.com' }
];