import React from 'react';
import { 
    Grid,
    CssBaseline,
    Container,
    Paper, 
    FormControl,
    InputLabel,
    OutlinedInput,
    InputAdornment,
    IconButton,
    Button,
    Avatar 
} from '@material-ui/core';

import Visibility from '@material-ui/icons/Visibility';
import PersonIcon from '@material-ui/icons/Person';
import LockIcon from '@material-ui/icons/Lock';
import CustomPaper from '../../Shared/CustomPaper/CustomPaper';

import { makeStyles } from '@material-ui/core/styles';
import bg from '../../assets/images/bg-content-2.jpg';

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        height: '100vh',
        alignItems: 'center'
    },
    customFont: {
        fontSize: '12px',
        textDecoration: 'none'
    },
    paper: {
        padding: '1rem'
    },
    logo: {
        borderRadius: '50px',
        background: '#dadada',
        width: '60px',
        height: '60px',
        textAlign: 'center',
        lineHeight: '2.5'
    },
    link: {
        textDecoration: 'none',
        color: '#fff'
    },
    footer: {
        background: '#3F51B5', 
        borderRadius: '10px', 
        padding: '1rem 2rem', 
        color: '#fff',
        marginTop: '10rem'
    },
    iconColor: {
        color: '#0000008a'
    },
    formControl: {
        margin: '8px 0px'
    },
    button: {
        marginTop: '8px',
        fontWeight: 'bold'
    },
    avatarStyle: {
        width: '4rem', 
        height: '4rem'
    }
}));

function LoginPage(props) {
    const {isLogin, setIsLogin} = props;
    const classes = useStyles();

    return (
        <React.Fragment>
            <CssBaseline />
            <Container maxWidth="xl" style={{
                backgroundImage: `url(${bg})`, 
                backgroundSize: 'cover', 
                backgroundPosition: 'center', 
                backgroundRepeat: 'no-repeat', 
            }}>
                <Grid 
                    container
                    justify="center"
                    alignItems="center"
                    className={classes.root}
                >
                    <Grid 
                        item 
                        xs={12} 
                        lg={3}
                    >
                        <CustomPaper>
                            <Grid container justify="center">
                                <Grid item xs={12} align="center" style={{ marginBottom: '1.5rem' }}>
                                    <Avatar className={classes.avatarStyle} />
                                </Grid>
                                <Grid item xs={12}>
                                    <FormControl 
                                        //className={clsx(classes.margin, classes.textField)}
                                        size="small"
                                        variant="outlined"
                                        fullWidth={true}
                                        className={classes.formControl}
                                        >
                                        <InputLabel htmlFor="outlined-adornment-password">Username</InputLabel>
                                        <OutlinedInput
                                            //id="outlined-adornment-password"
                                            startAdornment={
                                                <InputAdornment 
                                                    style={{padding: '8px 0px'}}
                                                    position="start">
                                                    <PersonIcon 
                                                        className={classes.iconColor}
                                                    />
                                                </InputAdornment>
                                            }
                                            //type={values.showPassword ? 'text' : 'password'}
                                            //value={values.password}
                                            //onChange={handleChange('password')}
                                            labelWidth={70}
                                        />
                                    </FormControl>
                                    <FormControl 
                                        //className={clsx(classes.margin, classes.textField)} 
                                        size="small"
                                        variant="outlined"
                                        fullWidth={true}
                                        className={classes.formControl}
                                        >
                                        <InputLabel htmlFor="outlined-adornment-password">Password</InputLabel>
                                        <OutlinedInput
                                            id="outlined-adornment-password"
                                            //type={values.showPassword ? 'text' : 'password'}
                                            //value={values.password}
                                            //onChange={handleChange('password')}
                                            startAdornment={
                                                <InputAdornment position="start">
                                                    <LockIcon 
                                                        className={classes.iconColor}
                                                    />
                                                </InputAdornment>
                                            }
                                            endAdornment={
                                            <InputAdornment position="end">
                                                <IconButton
                                                    style={{ padding: '6px' }}
                                                    aria-label="toggle password visibility"
                                                    //onClick={handleClickShowPassword}
                                                    //onMouseDown={handleMouseDownPassword}
                                                    edge="end"
                                                >
                                                    <Visibility />
                                                {/*values.showPassword ? <Visibility /> : <VisibilityOff />*/}
                                                </IconButton>
                                            </InputAdornment>
                                            }
                                            labelWidth={70}
                                        />
                                    </FormControl>
                                    <Button 
                                        onClick={ () => setIsLogin(true) }
                                        className={classes.button}
                                        size="large" 
                                        variant="contained" 
                                        color="primary" 
                                        fullWidth={true} 
                                        disableElevation
                                    >
                                        Login
                                    </Button>
                                </Grid>
                            </Grid>
                        </CustomPaper>
                    </Grid>
                </Grid>
            </Container>
        </React.Fragment>
    );
}

export default LoginPage;
