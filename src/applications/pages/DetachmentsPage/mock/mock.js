import Avatar from '@material-ui/core/Avatar';
export const headerData = [
    {
      title: 'ID', field: 'dispatchmentId'
    },
    {
      title: 'Detachment Name', field: 'dispatchmentName'
    },
    {
      title: 'Employee Name', field: 'employeeName'
    },
    {
      title: 'Wage Rate', field: 'wageRate',
    },
    {
      title: 'Days Work', field: 'daysOfWork',
    },
    { 
      title: 'Basic Pay', field: 'basicPay' 
    },
    { 
        title: 'Overtime', 
        field: 'overtime',
    },
    {
      title: 'ND Day', field: 'ndDays',
    },
    {
      title: 'ND Pay', field: 'ndPay',
    },
    {
      title: 'Allow', field: 'allow',
    },
    {
      title: 'ADJT', field: 'adjt',
    },
    {
      title: 'Gross Pay', field: 'grossPay',
    },
    {
      title: 'SSS', field: 'sssDispatchment',
    },
    {
      title: 'PhilHealth', field: 'philhealthDispatchment',
    },
    {
      title: 'Pag-ibig', field: 'pagIbigDispatchment',
    },
    {
      title: 'HDMF-MPL', field: 'hmdfMpl',
    },
    {
      title: 'Cash Bond', field: 'cashBond',
    },
    {
      title: 'AR & OE', field: 'arAndOe',
    },
    {
      title: 'Death Contributions', field: 'deathContribution',
    },
    {
      title: 'Total Deductions', field: 'totalDeductions',
    },
    {
      title: 'Net Pay', field: 'netPay',
    },
  ];

export const tableData = [
  {
    dispatchmentId: 1,
    dispatchmentName: 'Cebu Services',
    employeeName: 'ABALAJON EDWARD B',
    wageRate: 805.50,
    daysOfWork: 14,
    basicPay: 11252.00,
    overtime: 0,
    ndDays: 14,
    ndPay: 845.78,
    allow: 0,
    adjt: 0,
    grossPay: 12097.78,
    sssDispatchment: 340.00,
    philhealthDispatchment: 150.00,
    pagIbigDispatchment: 100.00,
    hmdfMpl: 0,
    cashBond: 100.00,
    arAndOe: 0,
    deathContribution: 100,
    totalDeductions: 790.00,
    netPay: 11307.78
  },
  {
    dispatchmentId: 2,
    dispatchmentName: 'Pampanga Services',
    employeeName: 'ABALAJON EDWARD B',
    wageRate: 805.50,
    daysOfWork: 14,
    basicPay: 11252.00,
    overtime: 0,
    ndDays: 14,
    ndPay: 845.78,
    allow: 0,
    adjt: 0,
    grossPay: 12097.78,
    sssDispatchment: 340.00,
    philhealthDispatchment: 150.00,
    pagIbigDispatchment: 100.00,
    hmdfMpl: 0,
    cashBond: 100.00,
    arAndOe: 0,
    deathContribution: 100,
    totalDeductions: 790.00,
    netPay: 11307.78
  },
  {
    dispatchmentId: 3,
    dispatchmentName: 'Cavite Services',
    employeeName: 'ABALAJON EDWARD B',
    wageRate: 805.50,
    daysOfWork: 14,
    basicPay: 11252.00,
    overtime: 0,
    ndDays: 14,
    ndPay: 845.78,
    allow: 0,
    adjt: 0,
    grossPay: 12097.78,
    sssDispatchment: 340.00,
    philhealthDispatchment: 150.00,
    pagIbigDispatchment: 100.00,
    hmdfMpl: 0,
    cashBond: 100.00,
    arAndOe: 0,
    deathContribution: 100,
    totalDeductions: 790.00,
    netPay: 11307.78
  },
  {
    dispatchmentId: 4,
    dispatchmentName: 'Nueva Ecija Services Inc',
    employeeName: 'ABALAJON EDWARD B',
    wageRate: 805.50,
    daysOfWork: 14,
    basicPay: 11252.00,
    overtime: 0,
    ndDays: 14,
    ndPay: 845.78,
    allow: 0,
    adjt: 0,
    grossPay: 12097.78,
    sssDispatchment: 340.00,
    philhealthDispatchment: 150.00,
    pagIbigDispatchment: 100.00,
    hmdfMpl: 0,
    cashBond: 100.00,
    arAndOe: 0,
    deathContribution: 100,
    totalDeductions: 790.00,
    netPay: 11307.78
  },
  {
    dispatchmentId: 5,
    dispatchmentName: 'Mindanao Services Inc',
    employeeName: 'ABALAJON EDWARD B',
    wageRate: 805.50,
    daysOfWork: 14,
    basicPay: 11252.00,
    overtime: 0,
    ndDays: 14,
    ndPay: 845.78,
    allow: 0,
    adjt: 0,
    grossPay: 12097.78,
    sssDispatchment: 340.00,
    philhealthDispatchment: 150.00,
    pagIbigDispatchment: 100.00,
    hmdfMpl: 0,
    cashBond: 100.00,
    arAndOe: 0,
    deathContribution: 100,
    totalDeductions: 790.00,
    netPay: 11307.78
  },
  
];