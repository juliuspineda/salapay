import React from 'react';
import { Route, Switch } from "react-router-dom";

import MainNavigation from './components/MainNavigation';
import DashboardPage from '../DashboardPage/DashboardPage';
import EmployeesPage from '../EmployeesPage/EmployeesPage';
import LoginPage from '../LoginPage/LoginPage';
import PayrollPage from '../PayrollPage/PayrollPage';
import ReportsPage from '../ReportsPage/ReportsPage';
import SettingsPage from '../SettingsPage/SettingsPage';

import PayrollAccountsComponent from '../../components/PayrollAccountsComponent/PayrollAccountsComponent';
import EmployeeAccountsComponent from '../../components/EmployeeAccountsComponent/EmployeeAccountsComponent';
import UsersPage from '../UsersPage/UsersPage';
import OrganizationsPage from '../OrganizationsPage/OrganizationsPage';
import EarningsDeductionsPage from '../EarningsDeductionsPage/EarningsDeductionsPage';
import DetachmentsPage from '../DetachmentsPage/DetachmentsPage';
import ClientsPage from '../ClientsPage/ClientsPage';
import NotFoundPage from '../NotFoundPage/NotFoundPage';

function Navigation(props) {   
    const {isLogin, setIsLogin} = props;
    // let loginView = '';
    // if(isLogin === true) {
    //     loginView = 
    // }

    const loginShow = isLogin ? <Route exact path="/login" render={ props => <LoginPage {...props}/> }/> : '';
    return (
        <div>
            {
                loginShow
            }
            <MainNavigation setIsLogin={setIsLogin} history={props.history}>
                <main>
                    <Switch>
                        <Route exact path="/" render={ props => <DashboardPage {...props}/> }/> 
                        <Route exact path="/employees" render={ props => <EmployeesPage {...props}/> }/> 
                        <Route exact path="/employee/account" render={ props => <EmployeeAccountsComponent {...props}/> }/>
                        <Route exact path="/payroll" render={ props => <PayrollPage {...props}/> }/>
                        <Route exact path="/payroll/accounts" render={ props => <PayrollAccountsComponent {...props}/> }/>  
                        <Route exact path="/reports" render={ props => <ReportsPage {...props}/> }/> 
                        <Route exact path="/settings" render={ props => <SettingsPage {...props}/> }/>
                        <Route exact path="/users" render={ props => <UsersPage {...props}/> }/>
                        <Route exact path="/clients" render={ props => <OrganizationsPage {...props}/> }/>
                        <Route exact path="/earnings" render={ props => <EarningsDeductionsPage {...props}/> }/>
                        <Route exact path="/detachments" render={ props => <DetachmentsPage {...props}/> }/>
                        {/* <Route exact path="/clients" render={ props => <ClientsPage {...props}/> }/> */}
                        {/* <Route component={<NotFoundPage />} />                   */}
                    </Switch>
                </main>
            </MainNavigation>
        </div>
    )
}

export default Navigation;
