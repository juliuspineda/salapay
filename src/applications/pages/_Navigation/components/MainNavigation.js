import React, {useState, Fragment} from 'react'
import {makeStyles,withStyles,useTheme,fade} from '@material-ui/core/styles';
import clsx from 'clsx';

import {
    AppBar,
    Toolbar,
    Typography,
    Button,
    CssBaseline,
    Drawer,
    List,
    ListItem,
    ListItemIcon,
    ListItemText,
    Divider,
    Grid,
    Avatar,
    Badge,
    IconButton,
    TextField
} from '@material-ui/core';

import SearchIcon from '@material-ui/icons/Search';
import InputBase from '@material-ui/core/InputBase';
import Tooltip from '@material-ui/core/Tooltip';

import DashboardOutlinedIcon from '@material-ui/icons/DashboardOutlined';
import PersonIcon from '@material-ui/icons/Person';
import LocalAtmIcon from '@material-ui/icons/LocalAtm';
import DescriptionIcon from '@material-ui/icons/Description';
import PeopleAltOutlinedIcon from '@material-ui/icons/PeopleAltOutlined';
import ContactSupportIcon from '@material-ui/icons/ContactSupport';
import ExitToAppOutlinedIcon from '@material-ui/icons/ExitToAppOutlined';
import SettingsIcon from '@material-ui/icons/Settings';
import PeopleIcon from '@material-ui/icons/People';

import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import MenuIcon from '@material-ui/icons/Menu';
import LocalShippingIcon from '@material-ui/icons/LocalShipping';
import CopyrightIcon from '@material-ui/icons/Copyright';
import NotificationsNoneIcon from '@material-ui/icons/NotificationsNone';

//const iconImg = require('../../../../assets/images/CASTER.png');
// const avatarImg = require('../../../assets/images/img_avatar.png');
import imgAvatar from '../../../assets/images/avatar2.png';

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex'
    },
    appBar: {
        zIndex: theme.zIndex.drawer + 1,
        transition: theme.transitions.create(['width', 'margin'], {
          easing: theme.transitions.easing.sharp,
          duration: theme.transitions.duration.leavingScreen,
        }),
        boxShadow: 'none'
      },
      appBarShift: {
        marginLeft: drawerWidth,
        width: `calc(100% - ${drawerWidth}px)`,
        transition: theme.transitions.create(['width', 'margin'], {
          easing: theme.transitions.easing.sharp,
          duration: theme.transitions.duration.enteringScreen,
        }),
      },
      menuButton: {
        marginRight: 30,
      },
      hide: {
        display: 'none',
      },
      drawer: {
        width: drawerWidth,
        flexShrink: 0,
        //whiteSpace: 'nowrap',
      },
      drawerOpen: {
        width: drawerWidth,
        transition: theme.transitions.create('width', {
          easing: theme.transitions.easing.sharp,
          duration: theme.transitions.duration.enteringScreen,
        }),
        background: '#233044'
      },
      drawerClose: {
        transition: theme.transitions.create('width', {
          easing: theme.transitions.easing.sharp,
          duration: theme.transitions.duration.leavingScreen,
        }),
        overflowX: 'hidden',
        width: theme.spacing(9) + 1,
        [theme.breakpoints.up('sm')]: {
        //   width: theme.spacing(9) + 1,
        },
        background: '#233044'
      },
    toolbar: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-start',
        minHeight: '30px',
        padding: theme.spacing(0, 3),
        color: '#fff',
        // necessary for content to be below app bar
        ...theme.mixins.toolbar,
    },
    content: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.default,
        padding: theme.spacing(1.5),
    },
    dashboardPaper: {
        borderRadius: '5px',
        padding: '10px'
    },
    large: {
        width: theme.spacing(10),
        height: theme.spacing(10),
    },
    medium: {
        width: '70px',
        height: '70px',
    },
    smallIcon: {
        width: '30px',
        height: '30px',
    },
    // defaultColor: {
    //     color: '#fff'
    // },
    searchContainer: {
        marginTop: '.5rem'
    },
    searchInput: {
        background: '#fff'
    },
    contentPaper: {
        height: '300px',
        padding: theme.spacing(2)
    },
    search: {
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: fade(theme.palette.common.white, 0.15),
        '&:hover': {
          backgroundColor: fade(theme.palette.common.white, 0.25),
        },
        marginRight: theme.spacing(2),
        marginLeft: 0,
        width: '100%',
        [theme.breakpoints.up('sm')]: {
          marginLeft: theme.spacing(3),
          width: 'auto',
        },
      },
      searchIcon: {
        padding: theme.spacing(0, 2),
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
      },
      inputRoot: {
        color: 'inherit',
      },
      inputInput: {
        padding: theme.spacing(1, 1, 1, 0),
        // vertical padding + font size from searchIcon
        paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('md')]: {
          width: '20ch',
        },
      },
      icons: {
        width: '1.5rem',
        height: '1.5rem',
        color: '#b8c0cd'
      },
      header: {
        background: '#fff'
      },
      text: {
        color: '#b8c0cd'
      },
      avatarStyle: {
        width: '2.5rem', 
        height: '2.5rem'
      }
}));

// styled-components
const StyledListItem = withStyles((theme) => ({
    button: {
        '&:hover': {
            borderRight: '3px solid #3F51B5',
            // borderRadius: '4px',
            color: '#3F51B5',
            background: '#0c2340'
        },
        '&:selected': {
            color: '#3F51B5'
        },
        paddingLeft: '23px',
        paddingTop: '1rem',
        paddingBottom: '1rem'
    }
}))(ListItem);

const StyledBadge = withStyles((theme) => ({
    badge: {
      backgroundColor: '#44b700',
      color: '#44b700',
      boxShadow: `0 0 0 2px ${theme.palette.background.paper}`,
      '&::after': {
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        borderRadius: '50%',
        animation: '$ripple 1.2s infinite ease-in-out',
        border: '1px solid currentColor',
        content: '""',
      },
    },
    '@keyframes ripple': {
      '0%': {
        transform: 'scale(.8)',
        opacity: 1,
      },
      '100%': {
        transform: 'scale(2.4)',
        opacity: 0,
      },
    },
}))(Badge);

function MainNavigation(props) {
    const { isLogin, setIsLogin } = props;
    console.log('props main', props.history.location.pathname)
    const classes = useStyles();
    const theme = useTheme();

    const [open, setOpen] = useState(true);

    const handleDrawerOpen = () => {
        setOpen(true);
    };

    const handleDrawerClose = () => {
        setOpen(false);
    };

    console.log('main',props.children)
    const [title] = useState('Salapay');

    const navList = [
        {
            text: 'Dashboard',
            url: '/' 
        },
        {
            text: 'Employees',
            url: '/employees' 
        },
        {
            text: 'Payroll',
            url: '/payroll' 
        },
        {
            text: 'Reports',
            url: '/reports' 
        },
        {
            text: 'Detachments',
            url: '/detachments'
        },
        {
            text: 'Clients',
            url: '/clients'
        },
        {
            text: 'Settings',
            url: '/settings' 
        },
    ];

    return (
        <div className={classes.root}>
            <CssBaseline />
            <AppBar 
                position="fixed" 
                className={clsx(classes.appBar, {
                    [classes.appBarShift]: open,
                })}
            >
                <Toolbar className={classes.header}>
                    <Grid item lg={6} md={6} sm={6} xs={6}>
                        <div style={{display:'flex'}}>
                            <div style={{alignSelf: 'center'}}>
                                <IconButton
                                    color="inherit"
                                    aria-label="open drawer"
                                    onClick={handleDrawerOpen}
                                    edge="start"
                                    className={clsx(classes.menuButton, {
                                    [classes.hide]: open,
                                    })}
                                >
                                    <MenuIcon className={classes.text} />
                                </IconButton>
                            </div>
                            <Grid container alignItems="center">
                                {/* <Typography variant="h5">{title}</Typography> */}
                                {/* <TextField
                                    id="outlined-full-width"
                                    label="Search"
                                    style={{ margin: 3 }}
                                    // placeholder="Search"
                                    //helperText="Full width!"
                                    //fullWidth
                                    margin="normal"
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    variant="outlined"
                                /> */}
                            </Grid>
                        {/* <div>
                            <Avatar alt="Caster" src={iconImg} className={classes.medium}/>
                        </div> */}
                        </div>
                    </Grid>
                    <Grid item lg={6} md={6} sm={6} xs={6}>
                        <Grid container>
                            <Grid item xs={6} style={{ alignSelf: 'center' }}>
                                {/* <div className={classes.search}>
                                    <div className={classes.searchIcon}>
                                    <SearchIcon />
                                    </div>
                                    <InputBase
                                    placeholder="Search…"
                                    classes={{
                                        root: classes.inputRoot,
                                        input: classes.inputInput,
                                    }}
                                    inputProps={{ 'aria-label': 'search' }}
                                    />
                                </div> */}
                            </Grid>
                            <Grid item xs={6} style={{ textAlign: 'end' }}>
                                <div>
                                    <IconButton 
                                        onClick={ () => { 
                                            //setIsLogin(false);
                                            //props.history.push('/');
                                        }}
                                        disableElevation
                                    >
                                        <Tooltip title="Reminder">
                                            <NotificationsNoneIcon className={classes.smallIcon}/>
                                        </Tooltip>
                                    </IconButton>
                                    <IconButton 
                                        onClick={ () => { 
                                            //setIsLogin(false);
                                            //props.history.push('/');
                                        }}
                                        disableElevation
                                    >
                                        <Tooltip title="Profile">
                                            <Avatar alt="Anna" src={imgAvatar} className={classes.smallIcon} />
                                        </Tooltip>
                                    </IconButton>
                                    <IconButton 
                                        onClick={ () => { 
                                            setIsLogin(false);
                                            props.history.push('/');
                                        }}
                                        disableElevation
                                    >
                                        <Tooltip title="Logout">
                                            <ExitToAppOutlinedIcon className={classes.smallIcon}/>
                                        </Tooltip>
                                    </IconButton>
                                </div>
                            </Grid>
                        </Grid>
                    </Grid>
                    
                </Toolbar>
            </AppBar>
            <Drawer
                variant="permanent"
                className={clsx(classes.drawer, {
                    [classes.drawerOpen]: open,
                    [classes.drawerClose]: !open,
                })}
                classes={{
                paper: clsx({
                    [classes.drawerOpen]: open,
                    [classes.drawerClose]: !open,
                    }),
                }}
            >
                <Grid>
                    <Grid className={classes.toolbar} alignItems="right">
                        <Typography variant="h5">{title}</Typography>
                        {/* <IconButton onClick={handleDrawerClose}>
                            {theme.direction === 'rtl' ? <ChevronRightIcon className={classes.text} /> : <ChevronLeftIcon className={classes.text} />}
                        </IconButton> */}
                    </Grid>
                    {/* <Grid align="center">
                        {
                            open && <Fragment>
                                <StyledBadge
                                    overlap="circle"
                                    anchorOrigin={{
                                    vertical: 'bottom',
                                    horizontal: 'right',
                                    }}
                                    variant="dot"
                                >
                                    <Avatar alt="Anna" src={imgAvatar} className={classes.avatarStyle} />
                                </StyledBadge>
                                    <Typography variant="h6" className={classes.text} style={{ marginTop: '.75rem' }}>Jasper</Typography>
                                    <Typography variant="body2" className={classes.text}>SUPER ADMIN</Typography>
                                <div className={classes.toolbar}/>
                            </Fragment>
                        }
                    </Grid> */}
                </Grid>
                {/* {
                    (open === true) ? <Divider /> : <div style={{marginTop: '1rem'}}/>
                } */}
                <List style={{padding: 0}}>
                {
                navList && navList.map((data, index) => (
                    <Tooltip title={open ? '' : <h3>{data.text}</h3>} placement="right-end" arrow>
                        <StyledListItem button key={index} onClick={ () => props.history.push(`${data.url}`) }>
                            <ListItemIcon style={{minWidth: '40px'}}>
                                    {
                                    (index === 0) ?
                                        <DashboardOutlinedIcon className={classes.icons} onClick={ () => props.history.push('/') }/>  
                                    : 
                                    (index === 1) ? 
                                        <PersonIcon className={classes.icons} onClick={ () => props.history.push('/employees') }/>
                                    :
                                    (index === 2) ? 
                                        <LocalAtmIcon className={classes.icons} onClick={ () => props.history.push('/payroll') }/>
                                    : 
                                    (index === 3) ? 
                                        <DescriptionIcon className={classes.icons} onClick={ () => props.history.push('/reports') }/>
                                    :
                                    (index === 4) ? 
                                        <LocalShippingIcon className={classes.icons} onClick={ () => props.history.push('/detachments') }/>
                                    : 
                                    (index === 5) ? 
                                        <PeopleIcon className={classes.icons} onClick={ () => props.history.push('/clients') }/>
                                    : 
                                    <SettingsIcon className={classes.icons} onClick={ () => props.history.push('/settings') }/>
                                    }
                                </ListItemIcon>
                            <ListItemText className={classes.text} primary={data.text} />
                        </StyledListItem>
                    </Tooltip>
                ))}
                </List>
                <Divider />
                <StyledListItem button>
                    <ListItemIcon style={{minWidth: '40px'}}>
                        <ContactSupportIcon className={classes.icons} />
                    </ListItemIcon>
                    <ListItemText className={classes.text} primary={'Support'}/>
                </StyledListItem>
                <Grid style={{
                    marginTop: '7.3rem', 
                    background: '#0c2340',
                    color: '#3F51B5',
                    padding: '1rem 23px',
                    display: 'flex'
                }}>
                    <StyledBadge
                        overlap="circle"
                        anchorOrigin={{
                        vertical: 'bottom',
                        horizontal: 'right',
                        }}
                        variant="dot"
                    >
                        <Avatar alt="Anna" src={imgAvatar} className={classes.avatarStyle} />
                    </StyledBadge>
                    <Grid style={{paddingLeft: '1rem'}}>
                        <Typography variant="subtitle1" style={{color: '#fff'}}>Justin Blake</Typography>
                        <Typography variant="subtitle2" style={{color: '#fff'}}>HR Officer</Typography>
                    </Grid>
                </Grid>
            </Drawer>
            <div style={{width: '100%', height: '100%'}}>
                {
                    props.children
                }
                {/* <Toolbar style={{background: '#fff', alignItems: 'center', justifyContent: 'flex-end'}}>
                    <CopyrightIcon style={{width: '15px', height: '15px', marginRight: '5px', color: 'gray'}} />
                    <Typography style={{color: 'gray'}}>2021 - Salapay</Typography>
                </Toolbar> */}
            </div>
        </div>
    )
}

export default MainNavigation;
