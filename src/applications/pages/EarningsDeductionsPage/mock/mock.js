import Avatar from '@material-ui/core/Avatar';
export const headerData = [
    {
      title: 'Staff', field: 'staff',
    },
    { title: 'Net Salary', field: 'netSalary' },
    { 
        title: 'Basic Salary', 
        field: 'basicSalary', 
        //type: 'numeric' 
    },
    { 
        title: 'Conveyance', 
        field: 'conveyance', 
        //type: 'numeric' 
    },
    { 
      title: 'Leave', 
      field: 'leave', 
      //type: 'numeric' 
  },
  { 
      title: 'Allowance', 
      field: 'allowance', 
      //type: 'numeric' 
  },
  { 
    title: 'SSS', 
    field: 'sss', 
    //type: 'numeric' 
  },
  { 
    title: 'DA(40%)', 
    field: 'da', 
    //type: 'numeric' 
  },
  { 
    title: 'Pag-Ibig', 
    field: 'pagIbig', 
    //type: 'numeric' 
  },
  { 
    title: 'HRA(15%)', 
    field: 'hra', 
    //type: 'numeric' 
  },
  { 
    title: 'Cash Advance', 
    field: 'cashAdvance', 
    //type: 'numeric' 
  },
  { 
    title: 'Medical Allowance', 
    field: 'medAllow', 
    //type: 'numeric' 
  },
  { 
    title: 'Loans', 
    field: 'loans', 
    //type: 'numeric' 
  }
  ];

export const tableData = [
    { staff: 'Jeff', netSalary: '15,000', basicSalary: '12,000', tds: '500', conveyance: '500', leave: '1,000', allowance: '200', sss: '123123123', da: '100', pagIbig: '300', hra: '100', cashAdvance: '', medAllow: '500', loans: '' },
    { staff: 'Jeff', netSalary: '15,000', basicSalary: '12,000', tds: '500', conveyance: '500', leave: '1,000', allowance: '200', sss: '123123123', da: '100', pagIbig: '300', hra: '100', cashAdvance: '', medAllow: '500', loans: '' },
    { staff: 'Jeff', netSalary: '15,000', basicSalary: '12,000', tds: '500', conveyance: '500', leave: '1,000', allowance: '200', sss: '123123123', da: '100', pagIbig: '300', hra: '100', cashAdvance: '', medAllow: '500', loans: '' },
    { staff: 'Jeff', netSalary: '15,000', basicSalary: '12,000', tds: '500', conveyance: '500', leave: '1,000', allowance: '200', sss: '123123123', da: '100', pagIbig: '300', hra: '100', cashAdvance: '', medAllow: '500', loans: '' },
    { staff: 'Jeff', netSalary: '15,000', basicSalary: '12,000', tds: '500', conveyance: '500', leave: '1,000', allowance: '200', sss: '123123123', da: '100', pagIbig: '300', hra: '100', cashAdvance: '', medAllow: '500', loans: '' }
];