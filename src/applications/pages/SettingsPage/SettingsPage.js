import React, {useState} from 'react'
import {
    Grid,
    Toolbar,
    Typography,
    Button
} from '@material-ui/core';

import ContactSupportIcon from '@material-ui/icons/ContactSupport';
import HeaderComponent from '../../components/HeaderComponent/HeaderComponent';
import BreadCrumbsComponent from '../../components/BreadCrumbsComponent/BreadCrumbsComponent';

import CustomDialog from '../../Shared/CustomDialog/CustomDialog';
import CustomContainer from '../../Shared/CustomContainer/CustomContainer';
import SuccessDialog from '../../Shared/SuccessDialog/SuccessDialog';   

import {makeStyles} from '@material-ui/core/styles';

import AddEmployeesComponent from '../../components/AddEmployeesComponent/AddEmployeesComponent';
import AddUsersComponent from '../../components/AddUsersComponent/AddUsersComponent';
import AddEarningsDeductionsComponent from '../../components/AddEarningsDeductionsComponent/AddEarningsDeductionsComponent';
import AddClientsComponent from '../../components/AddClientsComponent/AddClientsComponent';
import SetCutOffComponent from '../../components/SetCutOffComponent/SetCutOffComponent';

const useStyles = makeStyles((theme) => ({
    content: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.default,
        padding: theme.spacing(1.5),
    },
    contentPaper: {
        // height: '400px',
        padding: theme.spacing(3),
        alignItems: 'center',
        width: '100%',
    },
    text: {
        fontWeight:'700', 
        textTransform: 'none',
        padding: '.5rem 2rem'
    },
}));

function SettingsPage(props) {
    const classes = useStyles();

    const [employeesModal, setEmployeesModal] = useState(false);
    const [usersModal, setUsersModal] = useState(false);
    const [successModal, setSuccessModal] = useState(false);
    const [earningsModal, setEarningsModal] = useState(false);
    const [orgModal, setOrgModal] = useState(false);
    const [cutOffModal, setCutOffModal] = useState(false);

    const handleClickOpen = (content) => {
        switch(content) {
            case 'employees':
                return setEmployeesModal(true);
            case 'users':
                return setUsersModal(true);
            case 'earnings':
                return setEarningsModal(true);
            case 'organizations':
                return setOrgModal(true);
            case 'cutOff':
                return setCutOffModal(true);
            default:
                return '';
        }
    };
    const handleClose = () => {
        setEmployeesModal(false);
        setUsersModal(false);
        setEarningsModal(false);
        setOrgModal(false);
        setCutOffModal(false);
    };

    const handleOnSuccess = () => {
        setEmployeesModal(false);
        setUsersModal(false);
        setEarningsModal(false);
        setOrgModal(false);
        setCutOffModal(false);
        setSuccessModal(true);
    }

    const onClose = () => {
        setSuccessModal(false);
    }

    const modal = employeesModal ? employeesModal 
        : usersModal ? usersModal : earningsModal ? earningsModal : orgModal ? orgModal : cutOffModal ? cutOffModal : false;

    const children = employeesModal ? <AddEmployeesComponent/> 
        : usersModal ? <AddUsersComponent/> : earningsModal ? 
            <AddEarningsDeductionsComponent /> : orgModal ? <AddClientsComponent /> : setCutOffModal ? <SetCutOffComponent /> : '';
        
    const title = employeesModal ? 'Add Employees' : usersModal ? 'Add Users' : earningsModal ? 'Add New Earnings/Deductions' : orgModal ? 'Add New Organization' : cutOffModal ? 'Cut-Off' : '';

    return(
        <div>
            <Grid item xs={12} sm={12} md={12} lg={12} className={classes.content} style={{ padding: '0px' }}>
                <Toolbar/>
                <HeaderComponent />
            </Grid>
            <CustomContainer>
                <Grid xs={12} sm={12} md={12} lg={12}>
                    <Grid container spacing={2}>
                        <Grid item xs={12} sm={12} md={12} lg={12}>
                            <BreadCrumbsComponent title="Settings" label="Settings" />
                        </Grid>
                        <Grid item xs={12} sm={12} md={12} lg={12}>
                            <Grid container spacing={2}>
                                <Grid item xs={4}>
                                        <Grid container className={classes.contentPaper}>
                                            <Grid item xs={12} style={{ marginBottom: '1rem' }}>
                                                <Grid 
                                                    container
                                                    direction="column"
                                                    justify="center"
                                                    alignItems="flex-start"
                                                >
                                                    <Grid style={{marginBottom: '1rem'}}>
                                                        <Typography variant="h5">EMPLOYEES</Typography>
                                                    </Grid>
                                                    <Grid style={{marginBottom: '.75rem'}}>
                                                        <Button 
                                                            variant="contained" 
                                                            color="primary" 
                                                            disableElevation 
                                                            className={classes.text}
                                                            onClick={() => handleClickOpen('employees')}
                                                        >
                                                            Add New Employees
                                                        </Button>
                                                    </Grid>
                                                    <Grid>
                                                        <Button
                                                            onClick={ () => props.history.push('/employees') } 
                                                            variant="contained" 
                                                            color="primary" 
                                                            disableElevation 
                                                            className={classes.text}
                                                        >
                                                            Manage Employees
                                                        </Button>
                                                    </Grid>
                                                </Grid>
                                            </Grid>
                                        </Grid>
                                </Grid>
                                <Grid item xs={4}>
                                        <Grid container className={classes.contentPaper}>
                                            <Grid item xs={12} style={{ marginBottom: '1rem' }}>
                                                <Grid 
                                                    container
                                                    direction="column"
                                                    justify="center"
                                                    alignItems="flex-start"
                                                >
                                                    <Grid style={{marginBottom:'1rem'}}>
                                                        <Typography variant="h5">PAYROLL</Typography>
                                                    </Grid>
                                                    <Grid style={{marginBottom: '.75rem'}}>
                                                        <Button
                                                            onClick={ () => handleClickOpen('earnings') } 
                                                            variant="contained" 
                                                            color="primary" 
                                                            disableElevation 
                                                            className={classes.text}
                                                        >
                                                            Add New Earnings/Deductions
                                                        </Button>
                                                    </Grid>
                                                    <Grid style={{marginBottom: '.75rem'}}>
                                                        <Button
                                                            onClick={ () => props.history.push('/earnings') } 
                                                            variant="contained" 
                                                            color="primary" 
                                                            disableElevation 
                                                            className={classes.text}
                                                        >
                                                            Manage Earnings/Deductions
                                                        </Button>
                                                    </Grid>
                                                    <Grid style={{marginBottom: '.75rem'}}>
                                                        <Button
                                                            onClick={ () => props.history.push('/detachments') } 
                                                            variant="contained" 
                                                            color="primary" 
                                                            disableElevation 
                                                            className={classes.text}
                                                        >
                                                            Manage Detachments
                                                        </Button>
                                                    </Grid>
                                                    <Grid>
                                                        <Button
                                                            onClick={ () => handleClickOpen('cutOff') } 
                                                            variant="contained" 
                                                            color="primary" 
                                                            disableElevation 
                                                            className={classes.text}
                                                        >
                                                            Set Cut-off
                                                        </Button>
                                                    </Grid>
                                                </Grid>
                                            </Grid>
                                        </Grid>
                                </Grid>
                                {/* <Grid item xs={4}>
                                        <Grid container className={classes.contentPaper}>
                                            <Grid item xs={12} style={{ marginBottom: '1rem' }}>
                                                <Grid 
                                                    container
                                                    direction="column"
                                                    justify="center"
                                                    alignItems="flex-start"
                                                    spacing={2}
                                                >
                                                    <Grid style={{marginBottom:'1rem'}}>
                                                        <Typography variant="h5">USERS</Typography>
                                                    </Grid>
                                                    <Grid style={{marginBottom:'.75rem'}}>
                                                        <Button
                                                            onClick={ () => handleClickOpen('users') } 
                                                            variant="contained" 
                                                            color="primary" 
                                                            disableElevation 
                                                            className={classes.text}
                                                        >
                                                            Add New Users
                                                        </Button>
                                                    </Grid>
                                                    <Grid style={{marginBottom:'1rem'}}>
                                                        <Button
                                                            onClick={ () => props.history.push('/users') } 
                                                            variant="contained" 
                                                            color="primary" 
                                                            disableElevation 
                                                            className={classes.text}
                                                        >
                                                            Manage Users
                                                        </Button>
                                                    </Grid>
                                                    <Grid style={{marginBottom:'1rem'}}>
                                                        <Typography variant="h5">ORGANIZATIONS</Typography>
                                                    </Grid>
                                                    <Grid style={{marginBottom:'.75rem'}}>
                                                        <Button
                                                            onClick={ () => handleClickOpen('organizations') } 
                                                            variant="contained" 
                                                            color="primary" 
                                                            disableElevation 
                                                            className={classes.text}
                                                        >
                                                            Add New Organization
                                                        </Button>
                                                    </Grid>
                                                    <Grid>
                                                        <Button
                                                            onClick={ () => props.history.push('/organizations') } 
                                                            variant="contained" 
                                                            color="primary" 
                                                            disableElevation 
                                                            className={classes.text}
                                                        >
                                                            Manage Organization
                                                        </Button>
                                                    </Grid>
                                                </Grid>
                                            </Grid>
                                        </Grid>
                                </Grid> */}
                            </Grid>
                        </Grid>
                        <Grid item xs={12}>
                            <Grid container className={classes.contentPaper} style={{justifyContent: 'flex-end', marginTop: '5rem'}}>
                                <Button variant="contained" disableElevation style={{background: 'rgb(0, 196, 159)', padding: '1rem', fontWeight: '700', color: 'white'}}><ContactSupportIcon style={{marginRight:'1rem'}}/> REPORT AN ISSUE</Button>
                            </Grid>
                        </Grid>
                    </Grid>
                    <CustomDialog 
                        open={ modal }
                        onClose={handleClose}
                        children={ children }
                        title={ title }
                        onSuccess={ handleOnSuccess }
                        width={ earningsModal ? '800px' : '' }
                        showButton
                    />
                    <SuccessDialog 
                        open={successModal}
                        onClose={onClose}
                    />
                </Grid>
            </CustomContainer>
        </div>
    )
}

export default SettingsPage;